/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// require jQuery normally
const $ = require('jquery');

// create global $ and jQuery variables
global.$ = global.jQuery = $;

const routes = require('../public/js/fos_js_routes.json');
const Routing = require('../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js');

Routing.setRoutingData(routes);

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// require jQuery normally
import 'boosted/dist/js/boosted.min.js';

// start the Stimulus application
import './bootstrap';