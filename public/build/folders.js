"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["folders"],{

/***/ "./assets/folders.js":
/*!***************************!*\
  !*** ./assets/folders.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var selectize_dist_css_selectize_default_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! selectize/dist/css/selectize.default.css */ "./node_modules/selectize/dist/css/selectize.default.css");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
__webpack_require__(/*! selectize/dist/js/standalone/selectize.min */ "./node_modules/selectize/dist/js/standalone/selectize.min.js");


$(".filters").selectize({
  delimiter: ',',
  plugins: ['remove_button'],
  closeAfterSelect: true
}).on('change', function () {
  console.log($(this));
});

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_jquery_dist_jquery_js","vendors-node_modules_selectize_dist_css_selectize_default_css-node_modules_selectize_dist_js_-1e8fb7"], () => (__webpack_exec__("./assets/folders.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9sZGVycy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQUEsbUJBQU8sQ0FBQyxnSEFBRCxDQUFQOztBQUVBO0FBRUFDLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY0MsU0FBZCxDQUF3QjtBQUNwQkMsRUFBQUEsU0FBUyxFQUFFLEdBRFM7QUFFcEJDLEVBQUFBLE9BQU8sRUFBRSxDQUFDLGVBQUQsQ0FGVztBQUdwQkMsRUFBQUEsZ0JBQWdCLEVBQUU7QUFIRSxDQUF4QixFQUlHQyxFQUpILENBSU0sUUFKTixFQUlnQixZQUFXO0FBQ3ZCQyxFQUFBQSxPQUFPLENBQUNDLEdBQVIsQ0FBWVAsQ0FBQyxDQUFDLElBQUQsQ0FBYjtBQUNILENBTkQiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvZm9sZGVycy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlKCdzZWxlY3RpemUvZGlzdC9qcy9zdGFuZGFsb25lL3NlbGVjdGl6ZS5taW4nKTtcblxuaW1wb3J0ICdzZWxlY3RpemUvZGlzdC9jc3Mvc2VsZWN0aXplLmRlZmF1bHQuY3NzJztcblxuJChcIi5maWx0ZXJzXCIpLnNlbGVjdGl6ZSh7XG4gICAgZGVsaW1pdGVyOiAnLCcsXG4gICAgcGx1Z2luczogWydyZW1vdmVfYnV0dG9uJ10sXG4gICAgY2xvc2VBZnRlclNlbGVjdDogdHJ1ZVxufSkub24oJ2NoYW5nZScsIGZ1bmN0aW9uKCkge1xuICAgIGNvbnNvbGUubG9nKCQodGhpcykpO1xufSk7Il0sIm5hbWVzIjpbInJlcXVpcmUiLCIkIiwic2VsZWN0aXplIiwiZGVsaW1pdGVyIiwicGx1Z2lucyIsImNsb3NlQWZ0ZXJTZWxlY3QiLCJvbiIsImNvbnNvbGUiLCJsb2ciXSwic291cmNlUm9vdCI6IiJ9