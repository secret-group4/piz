(self["webpackChunk"] = self["webpackChunk"] || []).push([["folder"],{

/***/ "./assets/folder.js":
/*!**************************!*\
  !*** ./assets/folder.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.starts-with.js */ "./node_modules/core-js/modules/es.string.starts-with.js");
/* harmony import */ var core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_starts_with_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js */ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js");
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var noty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! noty */ "./node_modules/noty/lib/noty.js");
/* harmony import */ var noty__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(noty__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_folder_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles/folder.scss */ "./assets/styles/folder.scss");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");


__webpack_require__(/*! @fortawesome/fontawesome-free/css/all.min.css */ "./node_modules/@fortawesome/fontawesome-free/css/all.min.css");

__webpack_require__(/*! noty/src/noty.scss */ "./node_modules/noty/src/noty.scss");

__webpack_require__(/*! noty/src/themes/relax.scss */ "./node_modules/noty/src/themes/relax.scss");

__webpack_require__(/*! @fortawesome/fontawesome-free/js/all.js */ "./node_modules/@fortawesome/fontawesome-free/js/all.js");




$(".phaseSelect").on('change', function () {
  $.ajax({
    type: 'POST',
    url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default().generate('update_phase_folder', {
      id: $(this).data('folder')
    }),
    data: {
      phase: $(this).val()
    },
    success: function success(data) {
      var type = data ? "success" : "error";
      var text = data ? "Phase mise à jour" : "Une erreur est survenue";
      new (noty__WEBPACK_IMPORTED_MODULE_2___default())({
        theme: 'relax',
        layout: 'topRight',
        text: text,
        type: type,
        progressBar: true,
        timeout: 3000
      }).show();
    }
  });
});
$("#add_comment_cta").on('click', function () {
  $("#add_comment").toggleClass('d-none');
  $("html, body").animate({
    scrollTop: $(document).height()
  }, 1000);
});
$(".delete_comment_cta").on('click', function () {
  if (confirm('Êtes vous sur de vouloir supprimer ce commentaire ?')) {
    $.ajax({
      type: "POST",
      url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default().generate('folder_delete_comment', {
        id: $(this).data('comment')
      }),
      success: function success() {
        location.reload();
      }
    });
  }
});
$(".edit_comment_cta").on('click', function () {
  var id_comment = $(this).data('comment');
  var parentDiv = $('div[data-id="' + id_comment + '"]');
  parentDiv.toggleClass('d-none');
  $("#post_comment_pilot_scope").attr('disabled', function (_, attr) {
    return !attr;
  });
  $.ajax({
    type: "GET",
    url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default().generate('get_ids_scope_by_comment', {
      id: id_comment
    }),
    success: function success(ids) {
      $.ajax({
        type: "GET",
        url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default().generate('folder_get_comment', {
          id: id_comment
        }),
        success: function success(data) {
          $("#post_comment_pilot_comment").val(data.comment);

          if (data.scope.startsWith('FI')) {
            $("#post_comment_pilot_scope").val('FI');
          }

          if (data.scope.startsWith('FIS')) {
            $("#post_comment_pilot_scope").val('FIS');
          }

          if (data.scope.startsWith('IMB')) {
            $("#post_comment_pilot_scope").val('imb');
          }

          $("#add_comment").toggleClass('d-none');
          $("html, body").animate({
            scrollTop: $(document).height()
          }, 1000);
          $("#post_comment_pilot_submit").text('Modifier');
          $("#post_comment_pilot_submit").on('click', function (event) {
            event.preventDefault();
            $.ajax({
              type: "POST",
              url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_1___default().generate('edit_folder_comment', {
                id: id_comment
              }),
              data: {
                comment: $("#post_comment_pilot_comment").val(),
                ids: ids
              },
              success: function success() {
                location.reload();
              }
            });
          });
        }
      });
    }
  });
});

/***/ }),

/***/ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js":
/*!************************************************************************************!*\
  !*** ./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js ***!
  \************************************************************************************/
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.object.assign.js */ "./node_modules/core-js/modules/es.object.assign.js");

__webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.define-property.js */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.freeze.js */ "./node_modules/core-js/modules/es.object.freeze.js");

__webpack_require__(/*! core-js/modules/es.regexp.constructor.js */ "./node_modules/core-js/modules/es.regexp.constructor.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.regexp.to-string.js */ "./node_modules/core-js/modules/es.regexp.to-string.js");

__webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.index-of.js */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.object.keys.js */ "./node_modules/core-js/modules/es.object.keys.js");

__webpack_require__(/*! core-js/modules/es.array.join.js */ "./node_modules/core-js/modules/es.array.join.js");

__webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (e, t) {
  var n = t();
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (n.Routing),
		__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
		(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
		__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : 0;
}(this, function () {
  "use strict";

  function e(e, t) {
    if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
  }

  var t = Object.assign || function (e) {
    for (var t = 1; t < arguments.length; t++) {
      var n = arguments[t];

      for (var o in n) {
        Object.prototype.hasOwnProperty.call(n, o) && (e[o] = n[o]);
      }
    }

    return e;
  },
      n = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (e) {
    return _typeof(e);
  } : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : _typeof(e);
  },
      o = function () {
    function e(e, t) {
      for (var n = 0; n < t.length; n++) {
        var o = t[n];
        o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
      }
    }

    return function (t, n, o) {
      return n && e(t.prototype, n), o && e(t, o), t;
    };
  }(),
      r = function () {
    function r(t, n) {
      e(this, r), this.context_ = t || {
        base_url: "",
        prefix: "",
        host: "",
        port: "",
        scheme: "",
        locale: ""
      }, this.setRoutes(n || {});
    }

    return o(r, [{
      key: "setRoutingData",
      value: function value(e) {
        this.setBaseUrl(e.base_url), this.setRoutes(e.routes), "prefix" in e && this.setPrefix(e.prefix), "port" in e && this.setPort(e.port), "locale" in e && this.setLocale(e.locale), this.setHost(e.host), this.setScheme(e.scheme);
      }
    }, {
      key: "setRoutes",
      value: function value(e) {
        this.routes_ = Object.freeze(e);
      }
    }, {
      key: "getRoutes",
      value: function value() {
        return this.routes_;
      }
    }, {
      key: "setBaseUrl",
      value: function value(e) {
        this.context_.base_url = e;
      }
    }, {
      key: "getBaseUrl",
      value: function value() {
        return this.context_.base_url;
      }
    }, {
      key: "setPrefix",
      value: function value(e) {
        this.context_.prefix = e;
      }
    }, {
      key: "setScheme",
      value: function value(e) {
        this.context_.scheme = e;
      }
    }, {
      key: "getScheme",
      value: function value() {
        return this.context_.scheme;
      }
    }, {
      key: "setHost",
      value: function value(e) {
        this.context_.host = e;
      }
    }, {
      key: "getHost",
      value: function value() {
        return this.context_.host;
      }
    }, {
      key: "setPort",
      value: function value(e) {
        this.context_.port = e;
      }
    }, {
      key: "getPort",
      value: function value() {
        return this.context_.port;
      }
    }, {
      key: "setLocale",
      value: function value(e) {
        this.context_.locale = e;
      }
    }, {
      key: "getLocale",
      value: function value() {
        return this.context_.locale;
      }
    }, {
      key: "buildQueryParams",
      value: function value(e, t, o) {
        var r = this,
            i = void 0,
            u = new RegExp(/\[\]$/);
        if (t instanceof Array) t.forEach(function (t, i) {
          u.test(e) ? o(e, t) : r.buildQueryParams(e + "[" + ("object" === ("undefined" == typeof t ? "undefined" : n(t)) ? i : "") + "]", t, o);
        });else if ("object" === ("undefined" == typeof t ? "undefined" : n(t))) for (i in t) {
          this.buildQueryParams(e + "[" + i + "]", t[i], o);
        } else o(e, t);
      }
    }, {
      key: "getRoute",
      value: function value(e) {
        var t = this.context_.prefix + e,
            n = e + "." + this.context_.locale,
            o = this.context_.prefix + e + "." + this.context_.locale,
            r = [t, n, o, e];

        for (var i in r) {
          if (r[i] in this.routes_) return this.routes_[r[i]];
        }

        throw new Error('The route "' + e + '" does not exist.');
      }
    }, {
      key: "generate",
      value: function value(e, n) {
        var o = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
            i = this.getRoute(e),
            u = n || {},
            s = t({}, u),
            c = "",
            a = !0,
            l = "",
            f = "undefined" == typeof this.getPort() || null === this.getPort() ? "" : this.getPort();

        if (i.tokens.forEach(function (t) {
          if ("text" === t[0]) return c = r.encodePathComponent(t[1]) + c, void (a = !1);
          {
            if ("variable" !== t[0]) throw new Error('The token type "' + t[0] + '" is not supported.');
            var n = i.defaults && t[3] in i.defaults;

            if (!1 === a || !n || t[3] in u && u[t[3]] != i.defaults[t[3]]) {
              var o = void 0;
              if (t[3] in u) o = u[t[3]], delete s[t[3]];else {
                if (!n) {
                  if (a) return;
                  throw new Error('The route "' + e + '" requires the parameter "' + t[3] + '".');
                }

                o = i.defaults[t[3]];
              }
              var l = !0 === o || !1 === o || "" === o;

              if (!l || !a) {
                var f = r.encodePathComponent(o);
                "null" === f && null === o && (f = ""), c = t[1] + f + c;
              }

              a = !1;
            } else n && t[3] in s && delete s[t[3]];
          }
        }), "" === c && (c = "/"), i.hosttokens.forEach(function (e) {
          var t = void 0;
          return "text" === e[0] ? void (l = e[1] + l) : void ("variable" === e[0] && (e[3] in u ? (t = u[e[3]], delete s[e[3]]) : i.defaults && e[3] in i.defaults && (t = i.defaults[e[3]]), l = e[1] + t + l));
        }), c = this.context_.base_url + c, i.requirements && "_scheme" in i.requirements && this.getScheme() != i.requirements._scheme) {
          var h = l || this.getHost();
          c = i.requirements._scheme + "://" + h + (h.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c;
        } else if ("undefined" != typeof i.schemes && "undefined" != typeof i.schemes[0] && this.getScheme() !== i.schemes[0]) {
          var p = l || this.getHost();
          c = i.schemes[0] + "://" + p + (p.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c;
        } else l && this.getHost() !== l + (l.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) ? c = this.getScheme() + "://" + l + (l.indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c : o === !0 && (c = this.getScheme() + "://" + this.getHost() + (this.getHost().indexOf(":" + f) > -1 || "" === f ? "" : ":" + f) + c);

        if (Object.keys(s).length > 0) {
          var d = void 0,
              y = [],
              v = function v(e, t) {
            t = "function" == typeof t ? t() : t, t = null === t ? "" : t, y.push(r.encodeQueryComponent(e) + "=" + r.encodeQueryComponent(t));
          };

          for (d in s) {
            this.buildQueryParams(d, s[d], v);
          }

          c = c + "?" + y.join("&");
        }

        return c;
      }
    }], [{
      key: "getInstance",
      value: function value() {
        return i;
      }
    }, {
      key: "setData",
      value: function value(e) {
        var t = r.getInstance();
        t.setRoutingData(e);
      }
    }, {
      key: "customEncodeURIComponent",
      value: function value(e) {
        return encodeURIComponent(e).replace(/%2F/g, "/").replace(/%40/g, "@").replace(/%3A/g, ":").replace(/%21/g, "!").replace(/%3B/g, ";").replace(/%2C/g, ",").replace(/%2A/g, "*").replace(/\(/g, "%28").replace(/\)/g, "%29").replace(/'/g, "%27");
      }
    }, {
      key: "encodePathComponent",
      value: function value(e) {
        return r.customEncodeURIComponent(e).replace(/%3D/g, "=").replace(/%2B/g, "+").replace(/%21/g, "!").replace(/%7C/g, "|");
      }
    }, {
      key: "encodeQueryComponent",
      value: function value(e) {
        return r.customEncodeURIComponent(e).replace(/%3F/g, "?");
      }
    }]), r;
  }();

  r.Route, r.Context;
  var i = new r();
  return {
    Router: r,
    Routing: i
  };
});

/***/ }),

/***/ "./assets/styles/folder.scss":
/*!***********************************!*\
  !*** ./assets/styles/folder.scss ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_jquery_dist_jquery_js","vendors-node_modules_core-js_modules_es_array_for-each_js-node_modules_core-js_modules_es_arr-20a9b1","vendors-node_modules_fortawesome_fontawesome-free_js_all_js-node_modules_core-js_modules_es_s-cb0c0e"], () => (__webpack_exec__("./assets/folder.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9sZGVyLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLG1CQUFPLENBQUMsbUhBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyw2REFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLDZFQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsdUdBQUQsQ0FBUDs7QUFFQTtBQUNBO0FBRUE7QUFFQUcsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQkMsRUFBbEIsQ0FBcUIsUUFBckIsRUFBK0IsWUFBWTtBQUN2Q0QsRUFBQUEsQ0FBQyxDQUFDRSxJQUFGLENBQU87QUFDSEMsSUFBQUEsSUFBSSxFQUFFLE1BREg7QUFFSEMsSUFBQUEsR0FBRyxFQUFFTiwySEFBQSxDQUFpQixxQkFBakIsRUFBd0M7QUFBQ1EsTUFBQUEsRUFBRSxFQUFFTixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFPLElBQVIsQ0FBYSxRQUFiO0FBQUwsS0FBeEMsQ0FGRjtBQUdIQSxJQUFBQSxJQUFJLEVBQUU7QUFDRkMsTUFBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFTLEdBQVI7QUFETCxLQUhIO0FBTUhDLElBQUFBLE9BQU8sRUFBRSxpQkFBVUgsSUFBVixFQUFnQjtBQUNyQixVQUFNSixJQUFJLEdBQUlJLElBQUQsR0FBUyxTQUFULEdBQXFCLE9BQWxDO0FBQ0EsVUFBTUksSUFBSSxHQUFJSixJQUFELEdBQVMsbUJBQVQsR0FBK0IseUJBQTVDO0FBRUEsVUFBSVIsNkNBQUosQ0FBUztBQUNMYSxRQUFBQSxLQUFLLEVBQUUsT0FERjtBQUVMQyxRQUFBQSxNQUFNLEVBQUUsVUFGSDtBQUdMRixRQUFBQSxJQUFJLEVBQUVBLElBSEQ7QUFJTFIsUUFBQUEsSUFBSSxFQUFFQSxJQUpEO0FBS0xXLFFBQUFBLFdBQVcsRUFBRSxJQUxSO0FBTUxDLFFBQUFBLE9BQU8sRUFBRTtBQU5KLE9BQVQsRUFPR0MsSUFQSDtBQVFIO0FBbEJFLEdBQVA7QUFvQkgsQ0FyQkQ7QUF1QkFoQixDQUFDLENBQUMsa0JBQUQsQ0FBRCxDQUFzQkMsRUFBdEIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUMxQ0QsRUFBQUEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQmlCLFdBQWxCLENBQThCLFFBQTlCO0FBQ0FqQixFQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCa0IsT0FBaEIsQ0FBd0I7QUFBQ0MsSUFBQUEsU0FBUyxFQUFFbkIsQ0FBQyxDQUFDb0IsUUFBRCxDQUFELENBQVlDLE1BQVo7QUFBWixHQUF4QixFQUEyRCxJQUEzRDtBQUNILENBSEQ7QUFLQXJCLENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCQyxFQUF6QixDQUE0QixPQUE1QixFQUFxQyxZQUFZO0FBQzdDLE1BQUlxQixPQUFPLENBQUMscURBQUQsQ0FBWCxFQUFvRTtBQUNoRXRCLElBQUFBLENBQUMsQ0FBQ0UsSUFBRixDQUFPO0FBQ0hDLE1BQUFBLElBQUksRUFBRSxNQURIO0FBRUhDLE1BQUFBLEdBQUcsRUFBRU4sMkhBQUEsQ0FBaUIsdUJBQWpCLEVBQTBDO0FBQUNRLFFBQUFBLEVBQUUsRUFBRU4sQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRTyxJQUFSLENBQWEsU0FBYjtBQUFMLE9BQTFDLENBRkY7QUFHSEcsTUFBQUEsT0FBTyxFQUFFLG1CQUFZO0FBQ2pCYSxRQUFBQSxRQUFRLENBQUNDLE1BQVQ7QUFDSDtBQUxFLEtBQVA7QUFPSDtBQUNKLENBVkQ7QUFZQXhCLENBQUMsQ0FBQyxtQkFBRCxDQUFELENBQXVCQyxFQUF2QixDQUEwQixPQUExQixFQUFtQyxZQUFZO0FBQzNDLE1BQU13QixVQUFVLEdBQUd6QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFPLElBQVIsQ0FBYSxTQUFiLENBQW5CO0FBQ0EsTUFBTW1CLFNBQVMsR0FBRzFCLENBQUMsQ0FBQyxrQkFBa0J5QixVQUFsQixHQUErQixJQUFoQyxDQUFuQjtBQUNBQyxFQUFBQSxTQUFTLENBQUNULFdBQVYsQ0FBc0IsUUFBdEI7QUFDQWpCLEVBQUFBLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCMkIsSUFBL0IsQ0FBb0MsVUFBcEMsRUFBZ0QsVUFBVUMsQ0FBVixFQUFhRCxJQUFiLEVBQW1CO0FBQy9ELFdBQU8sQ0FBQ0EsSUFBUjtBQUNILEdBRkQ7QUFHQTNCLEVBQUFBLENBQUMsQ0FBQ0UsSUFBRixDQUFPO0FBQ0hDLElBQUFBLElBQUksRUFBRSxLQURIO0FBRUhDLElBQUFBLEdBQUcsRUFBRU4sMkhBQUEsQ0FBaUIsMEJBQWpCLEVBQTZDO0FBQUNRLE1BQUFBLEVBQUUsRUFBRW1CO0FBQUwsS0FBN0MsQ0FGRjtBQUdIZixJQUFBQSxPQUFPLEVBQUUsaUJBQVVtQixHQUFWLEVBQWU7QUFDcEI3QixNQUFBQSxDQUFDLENBQUNFLElBQUYsQ0FBTztBQUNIQyxRQUFBQSxJQUFJLEVBQUUsS0FESDtBQUVIQyxRQUFBQSxHQUFHLEVBQUVOLDJIQUFBLENBQWlCLG9CQUFqQixFQUF1QztBQUFDUSxVQUFBQSxFQUFFLEVBQUVtQjtBQUFMLFNBQXZDLENBRkY7QUFHSGYsUUFBQUEsT0FBTyxFQUFFLGlCQUFVSCxJQUFWLEVBQWdCO0FBQ3JCUCxVQUFBQSxDQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQ1MsR0FBakMsQ0FBcUNGLElBQUksQ0FBQ3VCLE9BQTFDOztBQUNBLGNBQUl2QixJQUFJLENBQUN3QixLQUFMLENBQVdDLFVBQVgsQ0FBc0IsSUFBdEIsQ0FBSixFQUFpQztBQUM3QmhDLFlBQUFBLENBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCUyxHQUEvQixDQUFtQyxJQUFuQztBQUNIOztBQUNELGNBQUlGLElBQUksQ0FBQ3dCLEtBQUwsQ0FBV0MsVUFBWCxDQUFzQixLQUF0QixDQUFKLEVBQWtDO0FBQzlCaEMsWUFBQUEsQ0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JTLEdBQS9CLENBQW1DLEtBQW5DO0FBQ0g7O0FBQ0QsY0FBSUYsSUFBSSxDQUFDd0IsS0FBTCxDQUFXQyxVQUFYLENBQXNCLEtBQXRCLENBQUosRUFBa0M7QUFDOUJoQyxZQUFBQSxDQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQlMsR0FBL0IsQ0FBbUMsS0FBbkM7QUFDSDs7QUFFRFQsVUFBQUEsQ0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQmlCLFdBQWxCLENBQThCLFFBQTlCO0FBQ0FqQixVQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCa0IsT0FBaEIsQ0FBd0I7QUFBQ0MsWUFBQUEsU0FBUyxFQUFFbkIsQ0FBQyxDQUFDb0IsUUFBRCxDQUFELENBQVlDLE1BQVo7QUFBWixXQUF4QixFQUEyRCxJQUEzRDtBQUNBckIsVUFBQUEsQ0FBQyxDQUFDLDRCQUFELENBQUQsQ0FBZ0NXLElBQWhDLENBQXFDLFVBQXJDO0FBQ0FYLFVBQUFBLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDQyxFQUFoQyxDQUFtQyxPQUFuQyxFQUE0QyxVQUFVZ0MsS0FBVixFQUFpQjtBQUN6REEsWUFBQUEsS0FBSyxDQUFDQyxjQUFOO0FBQ0FsQyxZQUFBQSxDQUFDLENBQUNFLElBQUYsQ0FBTztBQUNIQyxjQUFBQSxJQUFJLEVBQUUsTUFESDtBQUVIQyxjQUFBQSxHQUFHLEVBQUVOLDJIQUFBLENBQWlCLHFCQUFqQixFQUF3QztBQUFDUSxnQkFBQUEsRUFBRSxFQUFFbUI7QUFBTCxlQUF4QyxDQUZGO0FBR0hsQixjQUFBQSxJQUFJLEVBQUU7QUFDRnVCLGdCQUFBQSxPQUFPLEVBQUU5QixDQUFDLENBQUMsNkJBQUQsQ0FBRCxDQUFpQ1MsR0FBakMsRUFEUDtBQUVGb0IsZ0JBQUFBLEdBQUcsRUFBRUE7QUFGSCxlQUhIO0FBT0huQixjQUFBQSxPQUFPLEVBQUUsbUJBQVk7QUFDakJhLGdCQUFBQSxRQUFRLENBQUNDLE1BQVQ7QUFDSDtBQVRFLGFBQVA7QUFXSCxXQWJEO0FBY0g7QUFoQ0UsT0FBUDtBQWtDSDtBQXRDRSxHQUFQO0FBd0NILENBL0NEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xEQSxDQUFDLFVBQVNXLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsTUFBSUMsQ0FBQyxHQUFDRCxDQUFDLEVBQVA7QUFBVSxVQUFzQ0UsaUNBQU8sRUFBRCxvQ0FBSUQsQ0FBQyxDQUFDdkMsT0FBTjtBQUFBO0FBQUE7QUFBQSxrR0FBNUMsR0FBMkQsQ0FBM0Q7QUFBMEssQ0FBbE0sQ0FBbU0sSUFBbk0sRUFBd00sWUFBVTtBQUFDOztBQUFhLFdBQVNxQyxDQUFULENBQVdBLENBQVgsRUFBYUMsQ0FBYixFQUFlO0FBQUMsUUFBRyxFQUFFRCxDQUFDLFlBQVlDLENBQWYsQ0FBSCxFQUFxQixNQUFNLElBQUlRLFNBQUosQ0FBYyxtQ0FBZCxDQUFOO0FBQXlEOztBQUFBLE1BQUlSLENBQUMsR0FBQ1MsTUFBTSxDQUFDQyxNQUFQLElBQWUsVUFBU1gsQ0FBVCxFQUFXO0FBQUMsU0FBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNXLFNBQVMsQ0FBQ0MsTUFBeEIsRUFBK0JaLENBQUMsRUFBaEMsRUFBbUM7QUFBQyxVQUFJQyxDQUFDLEdBQUNVLFNBQVMsQ0FBQ1gsQ0FBRCxDQUFmOztBQUFtQixXQUFJLElBQUlhLENBQVIsSUFBYVosQ0FBYjtBQUFlUSxRQUFBQSxNQUFNLENBQUNLLFNBQVAsQ0FBaUJDLGNBQWpCLENBQWdDQyxJQUFoQyxDQUFxQ2YsQ0FBckMsRUFBdUNZLENBQXZDLE1BQTRDZCxDQUFDLENBQUNjLENBQUQsQ0FBRCxHQUFLWixDQUFDLENBQUNZLENBQUQsQ0FBbEQ7QUFBZjtBQUFzRTs7QUFBQSxXQUFPZCxDQUFQO0FBQVMsR0FBdks7QUFBQSxNQUF3S0UsQ0FBQyxHQUFDLGNBQVksT0FBT2dCLE1BQW5CLElBQTJCLG9CQUFpQkEsTUFBTSxDQUFDQyxRQUF4QixDQUEzQixHQUE0RCxVQUFTbkIsQ0FBVCxFQUFXO0FBQUMsbUJBQWNBLENBQWQ7QUFBZ0IsR0FBeEYsR0FBeUYsVUFBU0EsQ0FBVCxFQUFXO0FBQUMsV0FBT0EsQ0FBQyxJQUFFLGNBQVksT0FBT2tCLE1BQXRCLElBQThCbEIsQ0FBQyxDQUFDb0IsV0FBRixLQUFnQkYsTUFBOUMsSUFBc0RsQixDQUFDLEtBQUdrQixNQUFNLENBQUNILFNBQWpFLEdBQTJFLFFBQTNFLFdBQTJGZixDQUEzRixDQUFQO0FBQW9HLEdBQW5YO0FBQUEsTUFBb1hjLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBU2QsQ0FBVCxDQUFXQSxDQUFYLEVBQWFDLENBQWIsRUFBZTtBQUFDLFdBQUksSUFBSUMsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDRCxDQUFDLENBQUNZLE1BQWhCLEVBQXVCWCxDQUFDLEVBQXhCLEVBQTJCO0FBQUMsWUFBSVksQ0FBQyxHQUFDYixDQUFDLENBQUNDLENBQUQsQ0FBUDtBQUFXWSxRQUFBQSxDQUFDLENBQUNPLFVBQUYsR0FBYVAsQ0FBQyxDQUFDTyxVQUFGLElBQWMsQ0FBQyxDQUE1QixFQUE4QlAsQ0FBQyxDQUFDUSxZQUFGLEdBQWUsQ0FBQyxDQUE5QyxFQUFnRCxXQUFVUixDQUFWLEtBQWNBLENBQUMsQ0FBQ1MsUUFBRixHQUFXLENBQUMsQ0FBMUIsQ0FBaEQsRUFBNkViLE1BQU0sQ0FBQ2MsY0FBUCxDQUFzQnhCLENBQXRCLEVBQXdCYyxDQUFDLENBQUNXLEdBQTFCLEVBQThCWCxDQUE5QixDQUE3RTtBQUE4RztBQUFDOztBQUFBLFdBQU8sVUFBU2IsQ0FBVCxFQUFXQyxDQUFYLEVBQWFZLENBQWIsRUFBZTtBQUFDLGFBQU9aLENBQUMsSUFBRUYsQ0FBQyxDQUFDQyxDQUFDLENBQUNjLFNBQUgsRUFBYWIsQ0FBYixDQUFKLEVBQW9CWSxDQUFDLElBQUVkLENBQUMsQ0FBQ0MsQ0FBRCxFQUFHYSxDQUFILENBQXhCLEVBQThCYixDQUFyQztBQUF1QyxLQUE5RDtBQUErRCxHQUFoUCxFQUF0WDtBQUFBLE1BQXltQnlCLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBU0EsQ0FBVCxDQUFXekIsQ0FBWCxFQUFhQyxDQUFiLEVBQWU7QUFBQ0YsTUFBQUEsQ0FBQyxDQUFDLElBQUQsRUFBTTBCLENBQU4sQ0FBRCxFQUFVLEtBQUtDLFFBQUwsR0FBYzFCLENBQUMsSUFBRTtBQUFDMkIsUUFBQUEsUUFBUSxFQUFDLEVBQVY7QUFBYUMsUUFBQUEsTUFBTSxFQUFDLEVBQXBCO0FBQXVCQyxRQUFBQSxJQUFJLEVBQUMsRUFBNUI7QUFBK0JDLFFBQUFBLElBQUksRUFBQyxFQUFwQztBQUF1Q0MsUUFBQUEsTUFBTSxFQUFDLEVBQTlDO0FBQWlEQyxRQUFBQSxNQUFNLEVBQUM7QUFBeEQsT0FBM0IsRUFBdUYsS0FBS0MsU0FBTCxDQUFlaEMsQ0FBQyxJQUFFLEVBQWxCLENBQXZGO0FBQTZHOztBQUFBLFdBQU9ZLENBQUMsQ0FBQ1ksQ0FBRCxFQUFHLENBQUM7QUFBQ0QsTUFBQUEsR0FBRyxFQUFDLGdCQUFMO0FBQXNCVSxNQUFBQSxLQUFLLEVBQUMsZUFBU25DLENBQVQsRUFBVztBQUFDLGFBQUtvQyxVQUFMLENBQWdCcEMsQ0FBQyxDQUFDNEIsUUFBbEIsR0FBNEIsS0FBS00sU0FBTCxDQUFlbEMsQ0FBQyxDQUFDcUMsTUFBakIsQ0FBNUIsRUFBcUQsWUFBV3JDLENBQVgsSUFBYyxLQUFLc0MsU0FBTCxDQUFldEMsQ0FBQyxDQUFDNkIsTUFBakIsQ0FBbkUsRUFBNEYsVUFBUzdCLENBQVQsSUFBWSxLQUFLdUMsT0FBTCxDQUFhdkMsQ0FBQyxDQUFDK0IsSUFBZixDQUF4RyxFQUE2SCxZQUFXL0IsQ0FBWCxJQUFjLEtBQUt3QyxTQUFMLENBQWV4QyxDQUFDLENBQUNpQyxNQUFqQixDQUEzSSxFQUFvSyxLQUFLUSxPQUFMLENBQWF6QyxDQUFDLENBQUM4QixJQUFmLENBQXBLLEVBQXlMLEtBQUtZLFNBQUwsQ0FBZTFDLENBQUMsQ0FBQ2dDLE1BQWpCLENBQXpMO0FBQWtOO0FBQTFQLEtBQUQsRUFBNlA7QUFBQ1AsTUFBQUEsR0FBRyxFQUFDLFdBQUw7QUFBaUJVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsYUFBSzJDLE9BQUwsR0FBYWpDLE1BQU0sQ0FBQ2tDLE1BQVAsQ0FBYzVDLENBQWQsQ0FBYjtBQUE4QjtBQUFqRSxLQUE3UCxFQUFnVTtBQUFDeUIsTUFBQUEsR0FBRyxFQUFDLFdBQUw7QUFBaUJVLE1BQUFBLEtBQUssRUFBQyxpQkFBVTtBQUFDLGVBQU8sS0FBS1EsT0FBWjtBQUFvQjtBQUF0RCxLQUFoVSxFQUF3WDtBQUFDbEIsTUFBQUEsR0FBRyxFQUFDLFlBQUw7QUFBa0JVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsYUFBSzJCLFFBQUwsQ0FBY0MsUUFBZCxHQUF1QjVCLENBQXZCO0FBQXlCO0FBQTdELEtBQXhYLEVBQXViO0FBQUN5QixNQUFBQSxHQUFHLEVBQUMsWUFBTDtBQUFrQlUsTUFBQUEsS0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBTyxLQUFLUixRQUFMLENBQWNDLFFBQXJCO0FBQThCO0FBQWpFLEtBQXZiLEVBQTBmO0FBQUNILE1BQUFBLEdBQUcsRUFBQyxXQUFMO0FBQWlCVSxNQUFBQSxLQUFLLEVBQUMsZUFBU25DLENBQVQsRUFBVztBQUFDLGFBQUsyQixRQUFMLENBQWNFLE1BQWQsR0FBcUI3QixDQUFyQjtBQUF1QjtBQUExRCxLQUExZixFQUFzakI7QUFBQ3lCLE1BQUFBLEdBQUcsRUFBQyxXQUFMO0FBQWlCVSxNQUFBQSxLQUFLLEVBQUMsZUFBU25DLENBQVQsRUFBVztBQUFDLGFBQUsyQixRQUFMLENBQWNLLE1BQWQsR0FBcUJoQyxDQUFyQjtBQUF1QjtBQUExRCxLQUF0akIsRUFBa25CO0FBQUN5QixNQUFBQSxHQUFHLEVBQUMsV0FBTDtBQUFpQlUsTUFBQUEsS0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBTyxLQUFLUixRQUFMLENBQWNLLE1BQXJCO0FBQTRCO0FBQTlELEtBQWxuQixFQUFrckI7QUFBQ1AsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZVUsTUFBQUEsS0FBSyxFQUFDLGVBQVNuQyxDQUFULEVBQVc7QUFBQyxhQUFLMkIsUUFBTCxDQUFjRyxJQUFkLEdBQW1COUIsQ0FBbkI7QUFBcUI7QUFBdEQsS0FBbHJCLEVBQTB1QjtBQUFDeUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZVUsTUFBQUEsS0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBTyxLQUFLUixRQUFMLENBQWNHLElBQXJCO0FBQTBCO0FBQTFELEtBQTF1QixFQUFzeUI7QUFBQ0wsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZVUsTUFBQUEsS0FBSyxFQUFDLGVBQVNuQyxDQUFULEVBQVc7QUFBQyxhQUFLMkIsUUFBTCxDQUFjSSxJQUFkLEdBQW1CL0IsQ0FBbkI7QUFBcUI7QUFBdEQsS0FBdHlCLEVBQTgxQjtBQUFDeUIsTUFBQUEsR0FBRyxFQUFDLFNBQUw7QUFBZVUsTUFBQUEsS0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBTyxLQUFLUixRQUFMLENBQWNJLElBQXJCO0FBQTBCO0FBQTFELEtBQTkxQixFQUEwNUI7QUFBQ04sTUFBQUEsR0FBRyxFQUFDLFdBQUw7QUFBaUJVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsYUFBSzJCLFFBQUwsQ0FBY00sTUFBZCxHQUFxQmpDLENBQXJCO0FBQXVCO0FBQTFELEtBQTE1QixFQUFzOUI7QUFBQ3lCLE1BQUFBLEdBQUcsRUFBQyxXQUFMO0FBQWlCVSxNQUFBQSxLQUFLLEVBQUMsaUJBQVU7QUFBQyxlQUFPLEtBQUtSLFFBQUwsQ0FBY00sTUFBckI7QUFBNEI7QUFBOUQsS0FBdDlCLEVBQXNoQztBQUFDUixNQUFBQSxHQUFHLEVBQUMsa0JBQUw7QUFBd0JVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXQyxDQUFYLEVBQWFhLENBQWIsRUFBZTtBQUFDLFlBQUlZLENBQUMsR0FBQyxJQUFOO0FBQUEsWUFBV21CLENBQUMsR0FBQyxLQUFLLENBQWxCO0FBQUEsWUFBb0JDLENBQUMsR0FBQyxJQUFJQyxNQUFKLENBQVcsT0FBWCxDQUF0QjtBQUEwQyxZQUFHOUMsQ0FBQyxZQUFZK0MsS0FBaEIsRUFBc0IvQyxDQUFDLENBQUNnRCxPQUFGLENBQVUsVUFBU2hELENBQVQsRUFBVzRDLENBQVgsRUFBYTtBQUFDQyxVQUFBQSxDQUFDLENBQUNJLElBQUYsQ0FBT2xELENBQVAsSUFBVWMsQ0FBQyxDQUFDZCxDQUFELEVBQUdDLENBQUgsQ0FBWCxHQUFpQnlCLENBQUMsQ0FBQ3lCLGdCQUFGLENBQW1CbkQsQ0FBQyxHQUFDLEdBQUYsSUFBTyxjQUFZLGVBQWEsT0FBT0MsQ0FBcEIsR0FBc0IsV0FBdEIsR0FBa0NDLENBQUMsQ0FBQ0QsQ0FBRCxDQUEvQyxJQUFvRDRDLENBQXBELEdBQXNELEVBQTdELElBQWlFLEdBQXBGLEVBQXdGNUMsQ0FBeEYsRUFBMEZhLENBQTFGLENBQWpCO0FBQThHLFNBQXRJLEVBQXRCLEtBQW1LLElBQUcsY0FBWSxlQUFhLE9BQU9iLENBQXBCLEdBQXNCLFdBQXRCLEdBQWtDQyxDQUFDLENBQUNELENBQUQsQ0FBL0MsQ0FBSCxFQUF1RCxLQUFJNEMsQ0FBSixJQUFTNUMsQ0FBVDtBQUFXLGVBQUtrRCxnQkFBTCxDQUFzQm5ELENBQUMsR0FBQyxHQUFGLEdBQU02QyxDQUFOLEdBQVEsR0FBOUIsRUFBa0M1QyxDQUFDLENBQUM0QyxDQUFELENBQW5DLEVBQXVDL0IsQ0FBdkM7QUFBWCxTQUF2RCxNQUFpSEEsQ0FBQyxDQUFDZCxDQUFELEVBQUdDLENBQUgsQ0FBRDtBQUFPO0FBQW5YLEtBQXRoQyxFQUEyNEM7QUFBQ3dCLE1BQUFBLEdBQUcsRUFBQyxVQUFMO0FBQWdCVSxNQUFBQSxLQUFLLEVBQUMsZUFBU25DLENBQVQsRUFBVztBQUFDLFlBQUlDLENBQUMsR0FBQyxLQUFLMEIsUUFBTCxDQUFjRSxNQUFkLEdBQXFCN0IsQ0FBM0I7QUFBQSxZQUE2QkUsQ0FBQyxHQUFDRixDQUFDLEdBQUMsR0FBRixHQUFNLEtBQUsyQixRQUFMLENBQWNNLE1BQW5EO0FBQUEsWUFBMERuQixDQUFDLEdBQUMsS0FBS2EsUUFBTCxDQUFjRSxNQUFkLEdBQXFCN0IsQ0FBckIsR0FBdUIsR0FBdkIsR0FBMkIsS0FBSzJCLFFBQUwsQ0FBY00sTUFBckc7QUFBQSxZQUE0R1AsQ0FBQyxHQUFDLENBQUN6QixDQUFELEVBQUdDLENBQUgsRUFBS1ksQ0FBTCxFQUFPZCxDQUFQLENBQTlHOztBQUF3SCxhQUFJLElBQUk2QyxDQUFSLElBQWFuQixDQUFiO0FBQWUsY0FBR0EsQ0FBQyxDQUFDbUIsQ0FBRCxDQUFELElBQU8sS0FBS0YsT0FBZixFQUF1QixPQUFPLEtBQUtBLE9BQUwsQ0FBYWpCLENBQUMsQ0FBQ21CLENBQUQsQ0FBZCxDQUFQO0FBQXRDOztBQUFnRSxjQUFNLElBQUlPLEtBQUosQ0FBVSxnQkFBY3BELENBQWQsR0FBZ0IsbUJBQTFCLENBQU47QUFBcUQ7QUFBL1EsS0FBMzRDLEVBQTRwRDtBQUFDeUIsTUFBQUEsR0FBRyxFQUFDLFVBQUw7QUFBZ0JVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXRSxDQUFYLEVBQWE7QUFBQyxZQUFJWSxDQUFDLEdBQUNGLFNBQVMsQ0FBQ0MsTUFBVixHQUFpQixDQUFqQixJQUFvQixLQUFLLENBQUwsS0FBU0QsU0FBUyxDQUFDLENBQUQsQ0FBdEMsSUFBMkNBLFNBQVMsQ0FBQyxDQUFELENBQTFEO0FBQUEsWUFBOERpQyxDQUFDLEdBQUMsS0FBS1EsUUFBTCxDQUFjckQsQ0FBZCxDQUFoRTtBQUFBLFlBQWlGOEMsQ0FBQyxHQUFDNUMsQ0FBQyxJQUFFLEVBQXRGO0FBQUEsWUFBeUZvRCxDQUFDLEdBQUNyRCxDQUFDLENBQUMsRUFBRCxFQUFJNkMsQ0FBSixDQUE1RjtBQUFBLFlBQW1HUyxDQUFDLEdBQUMsRUFBckc7QUFBQSxZQUF3R0MsQ0FBQyxHQUFDLENBQUMsQ0FBM0c7QUFBQSxZQUE2R0MsQ0FBQyxHQUFDLEVBQS9HO0FBQUEsWUFBa0hDLENBQUMsR0FBQyxlQUFhLE9BQU8sS0FBS0MsT0FBTCxFQUFwQixJQUFvQyxTQUFPLEtBQUtBLE9BQUwsRUFBM0MsR0FBMEQsRUFBMUQsR0FBNkQsS0FBS0EsT0FBTCxFQUFqTDs7QUFBZ00sWUFBR2QsQ0FBQyxDQUFDZSxNQUFGLENBQVNYLE9BQVQsQ0FBaUIsVUFBU2hELENBQVQsRUFBVztBQUFDLGNBQUcsV0FBU0EsQ0FBQyxDQUFDLENBQUQsQ0FBYixFQUFpQixPQUFPc0QsQ0FBQyxHQUFDN0IsQ0FBQyxDQUFDbUMsbUJBQUYsQ0FBc0I1RCxDQUFDLENBQUMsQ0FBRCxDQUF2QixJQUE0QnNELENBQTlCLEVBQWdDLE1BQUtDLENBQUMsR0FBQyxDQUFDLENBQVIsQ0FBdkM7QUFBa0Q7QUFBQyxnQkFBRyxlQUFhdkQsQ0FBQyxDQUFDLENBQUQsQ0FBakIsRUFBcUIsTUFBTSxJQUFJbUQsS0FBSixDQUFVLHFCQUFtQm5ELENBQUMsQ0FBQyxDQUFELENBQXBCLEdBQXdCLHFCQUFsQyxDQUFOO0FBQStELGdCQUFJQyxDQUFDLEdBQUMyQyxDQUFDLENBQUNpQixRQUFGLElBQVk3RCxDQUFDLENBQUMsQ0FBRCxDQUFELElBQU80QyxDQUFDLENBQUNpQixRQUEzQjs7QUFBb0MsZ0JBQUcsQ0FBQyxDQUFELEtBQUtOLENBQUwsSUFBUSxDQUFDdEQsQ0FBVCxJQUFZRCxDQUFDLENBQUMsQ0FBRCxDQUFELElBQU82QyxDQUFQLElBQVVBLENBQUMsQ0FBQzdDLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBRCxJQUFTNEMsQ0FBQyxDQUFDaUIsUUFBRixDQUFXN0QsQ0FBQyxDQUFDLENBQUQsQ0FBWixDQUFsQyxFQUFtRDtBQUFDLGtCQUFJYSxDQUFDLEdBQUMsS0FBSyxDQUFYO0FBQWEsa0JBQUdiLENBQUMsQ0FBQyxDQUFELENBQUQsSUFBTzZDLENBQVYsRUFBWWhDLENBQUMsR0FBQ2dDLENBQUMsQ0FBQzdDLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBSCxFQUFVLE9BQU9xRCxDQUFDLENBQUNyRCxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQWxCLENBQVosS0FBeUM7QUFBQyxvQkFBRyxDQUFDQyxDQUFKLEVBQU07QUFBQyxzQkFBR3NELENBQUgsRUFBSztBQUFPLHdCQUFNLElBQUlKLEtBQUosQ0FBVSxnQkFBY3BELENBQWQsR0FBZ0IsNEJBQWhCLEdBQTZDQyxDQUFDLENBQUMsQ0FBRCxDQUE5QyxHQUFrRCxJQUE1RCxDQUFOO0FBQXdFOztBQUFBYSxnQkFBQUEsQ0FBQyxHQUFDK0IsQ0FBQyxDQUFDaUIsUUFBRixDQUFXN0QsQ0FBQyxDQUFDLENBQUQsQ0FBWixDQUFGO0FBQW1CO0FBQUEsa0JBQUl3RCxDQUFDLEdBQUMsQ0FBQyxDQUFELEtBQUszQyxDQUFMLElBQVEsQ0FBQyxDQUFELEtBQUtBLENBQWIsSUFBZ0IsT0FBS0EsQ0FBM0I7O0FBQTZCLGtCQUFHLENBQUMyQyxDQUFELElBQUksQ0FBQ0QsQ0FBUixFQUFVO0FBQUMsb0JBQUlFLENBQUMsR0FBQ2hDLENBQUMsQ0FBQ21DLG1CQUFGLENBQXNCL0MsQ0FBdEIsQ0FBTjtBQUErQiwyQkFBUzRDLENBQVQsSUFBWSxTQUFPNUMsQ0FBbkIsS0FBdUI0QyxDQUFDLEdBQUMsRUFBekIsR0FBNkJILENBQUMsR0FBQ3RELENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS3lELENBQUwsR0FBT0gsQ0FBdEM7QUFBd0M7O0FBQUFDLGNBQUFBLENBQUMsR0FBQyxDQUFDLENBQUg7QUFBSyxhQUE3VSxNQUFrVnRELENBQUMsSUFBRUQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFPcUQsQ0FBVixJQUFhLE9BQU9BLENBQUMsQ0FBQ3JELENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBckI7QUFBNEI7QUFBQyxTQUF4a0IsR0FBMGtCLE9BQUtzRCxDQUFMLEtBQVNBLENBQUMsR0FBQyxHQUFYLENBQTFrQixFQUEwbEJWLENBQUMsQ0FBQ2tCLFVBQUYsQ0FBYWQsT0FBYixDQUFxQixVQUFTakQsQ0FBVCxFQUFXO0FBQUMsY0FBSUMsQ0FBQyxHQUFDLEtBQUssQ0FBWDtBQUFhLGlCQUFNLFdBQVNELENBQUMsQ0FBQyxDQUFELENBQVYsR0FBYyxNQUFLeUQsQ0FBQyxHQUFDekQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLeUQsQ0FBWixDQUFkLEdBQTZCLE1BQUssZUFBYXpELENBQUMsQ0FBQyxDQUFELENBQWQsS0FBb0JBLENBQUMsQ0FBQyxDQUFELENBQUQsSUFBTzhDLENBQVAsSUFBVTdDLENBQUMsR0FBQzZDLENBQUMsQ0FBQzlDLENBQUMsQ0FBQyxDQUFELENBQUYsQ0FBSCxFQUFVLE9BQU9zRCxDQUFDLENBQUN0RCxDQUFDLENBQUMsQ0FBRCxDQUFGLENBQTVCLElBQW9DNkMsQ0FBQyxDQUFDaUIsUUFBRixJQUFZOUQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFPNkMsQ0FBQyxDQUFDaUIsUUFBckIsS0FBZ0M3RCxDQUFDLEdBQUM0QyxDQUFDLENBQUNpQixRQUFGLENBQVc5RCxDQUFDLENBQUMsQ0FBRCxDQUFaLENBQWxDLENBQXBDLEVBQXdGeUQsQ0FBQyxHQUFDekQsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFLQyxDQUFMLEdBQU93RCxDQUFySCxDQUFMLENBQW5DO0FBQWlLLFNBQS9NLENBQTFsQixFQUEyeUJGLENBQUMsR0FBQyxLQUFLNUIsUUFBTCxDQUFjQyxRQUFkLEdBQXVCMkIsQ0FBcDBCLEVBQXMwQlYsQ0FBQyxDQUFDbUIsWUFBRixJQUFnQixhQUFZbkIsQ0FBQyxDQUFDbUIsWUFBOUIsSUFBNEMsS0FBS0MsU0FBTCxNQUFrQnBCLENBQUMsQ0FBQ21CLFlBQUYsQ0FBZUUsT0FBdDVCLEVBQTg1QjtBQUFDLGNBQUlDLENBQUMsR0FBQ1YsQ0FBQyxJQUFFLEtBQUtXLE9BQUwsRUFBVDtBQUF3QmIsVUFBQUEsQ0FBQyxHQUFDVixDQUFDLENBQUNtQixZQUFGLENBQWVFLE9BQWYsR0FBdUIsS0FBdkIsR0FBNkJDLENBQTdCLElBQWdDQSxDQUFDLENBQUNFLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBbkUsSUFBc0VILENBQXhFO0FBQTBFLFNBQWpnQyxNQUFzZ0MsSUFBRyxlQUFhLE9BQU9WLENBQUMsQ0FBQ3lCLE9BQXRCLElBQStCLGVBQWEsT0FBT3pCLENBQUMsQ0FBQ3lCLE9BQUYsQ0FBVSxDQUFWLENBQW5ELElBQWlFLEtBQUtMLFNBQUwsT0FBbUJwQixDQUFDLENBQUN5QixPQUFGLENBQVUsQ0FBVixDQUF2RixFQUFvRztBQUFDLGNBQUlDLENBQUMsR0FBQ2QsQ0FBQyxJQUFFLEtBQUtXLE9BQUwsRUFBVDtBQUF3QmIsVUFBQUEsQ0FBQyxHQUFDVixDQUFDLENBQUN5QixPQUFGLENBQVUsQ0FBVixJQUFhLEtBQWIsR0FBbUJDLENBQW5CLElBQXNCQSxDQUFDLENBQUNGLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBekQsSUFBNERILENBQTlEO0FBQWdFLFNBQTdMLE1BQWtNRSxDQUFDLElBQUUsS0FBS1csT0FBTCxPQUFpQlgsQ0FBQyxJQUFFQSxDQUFDLENBQUNZLE9BQUYsQ0FBVSxNQUFJWCxDQUFkLElBQWlCLENBQUMsQ0FBbEIsSUFBcUIsT0FBS0EsQ0FBMUIsR0FBNEIsRUFBNUIsR0FBK0IsTUFBSUEsQ0FBckMsQ0FBckIsR0FBNkRILENBQUMsR0FBQyxLQUFLVSxTQUFMLEtBQWlCLEtBQWpCLEdBQXVCUixDQUF2QixJQUEwQkEsQ0FBQyxDQUFDWSxPQUFGLENBQVUsTUFBSVgsQ0FBZCxJQUFpQixDQUFDLENBQWxCLElBQXFCLE9BQUtBLENBQTFCLEdBQTRCLEVBQTVCLEdBQStCLE1BQUlBLENBQTdELElBQWdFSCxDQUEvSCxHQUFpSXpDLENBQUMsS0FBRyxDQUFDLENBQUwsS0FBU3lDLENBQUMsR0FBQyxLQUFLVSxTQUFMLEtBQWlCLEtBQWpCLEdBQXVCLEtBQUtHLE9BQUwsRUFBdkIsSUFBdUMsS0FBS0EsT0FBTCxHQUFlQyxPQUFmLENBQXVCLE1BQUlYLENBQTNCLElBQThCLENBQUMsQ0FBL0IsSUFBa0MsT0FBS0EsQ0FBdkMsR0FBeUMsRUFBekMsR0FBNEMsTUFBSUEsQ0FBdkYsSUFBMEZILENBQXJHLENBQWpJOztBQUF5TyxZQUFHN0MsTUFBTSxDQUFDOEQsSUFBUCxDQUFZbEIsQ0FBWixFQUFlekMsTUFBZixHQUFzQixDQUF6QixFQUEyQjtBQUFDLGNBQUk0RCxDQUFDLEdBQUMsS0FBSyxDQUFYO0FBQUEsY0FBYUMsQ0FBQyxHQUFDLEVBQWY7QUFBQSxjQUFrQkMsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBUzNFLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUNBLFlBQUFBLENBQUMsR0FBQyxjQUFZLE9BQU9BLENBQW5CLEdBQXFCQSxDQUFDLEVBQXRCLEdBQXlCQSxDQUEzQixFQUE2QkEsQ0FBQyxHQUFDLFNBQU9BLENBQVAsR0FBUyxFQUFULEdBQVlBLENBQTNDLEVBQTZDeUUsQ0FBQyxDQUFDRSxJQUFGLENBQU9sRCxDQUFDLENBQUNtRCxvQkFBRixDQUF1QjdFLENBQXZCLElBQTBCLEdBQTFCLEdBQThCMEIsQ0FBQyxDQUFDbUQsb0JBQUYsQ0FBdUI1RSxDQUF2QixDQUFyQyxDQUE3QztBQUE2RyxXQUEvSTs7QUFBZ0osZUFBSXdFLENBQUosSUFBU25CLENBQVQ7QUFBVyxpQkFBS0gsZ0JBQUwsQ0FBc0JzQixDQUF0QixFQUF3Qm5CLENBQUMsQ0FBQ21CLENBQUQsQ0FBekIsRUFBNkJFLENBQTdCO0FBQVg7O0FBQTJDcEIsVUFBQUEsQ0FBQyxHQUFDQSxDQUFDLEdBQUMsR0FBRixHQUFNbUIsQ0FBQyxDQUFDSSxJQUFGLENBQU8sR0FBUCxDQUFSO0FBQW9COztBQUFBLGVBQU92QixDQUFQO0FBQVM7QUFBejRELEtBQTVwRCxDQUFILEVBQTJpSCxDQUFDO0FBQUM5QixNQUFBQSxHQUFHLEVBQUMsYUFBTDtBQUFtQlUsTUFBQUEsS0FBSyxFQUFDLGlCQUFVO0FBQUMsZUFBT1UsQ0FBUDtBQUFTO0FBQTdDLEtBQUQsRUFBZ0Q7QUFBQ3BCLE1BQUFBLEdBQUcsRUFBQyxTQUFMO0FBQWVVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsWUFBSUMsQ0FBQyxHQUFDeUIsQ0FBQyxDQUFDcUQsV0FBRixFQUFOO0FBQXNCOUUsUUFBQUEsQ0FBQyxDQUFDK0UsY0FBRixDQUFpQmhGLENBQWpCO0FBQW9CO0FBQTNFLEtBQWhELEVBQTZIO0FBQUN5QixNQUFBQSxHQUFHLEVBQUMsMEJBQUw7QUFBZ0NVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsZUFBT2lGLGtCQUFrQixDQUFDakYsQ0FBRCxDQUFsQixDQUFzQmtGLE9BQXRCLENBQThCLE1BQTlCLEVBQXFDLEdBQXJDLEVBQTBDQSxPQUExQyxDQUFrRCxNQUFsRCxFQUF5RCxHQUF6RCxFQUE4REEsT0FBOUQsQ0FBc0UsTUFBdEUsRUFBNkUsR0FBN0UsRUFBa0ZBLE9BQWxGLENBQTBGLE1BQTFGLEVBQWlHLEdBQWpHLEVBQXNHQSxPQUF0RyxDQUE4RyxNQUE5RyxFQUFxSCxHQUFySCxFQUEwSEEsT0FBMUgsQ0FBa0ksTUFBbEksRUFBeUksR0FBekksRUFBOElBLE9BQTlJLENBQXNKLE1BQXRKLEVBQTZKLEdBQTdKLEVBQWtLQSxPQUFsSyxDQUEwSyxLQUExSyxFQUFnTCxLQUFoTCxFQUF1TEEsT0FBdkwsQ0FBK0wsS0FBL0wsRUFBcU0sS0FBck0sRUFBNE1BLE9BQTVNLENBQW9OLElBQXBOLEVBQXlOLEtBQXpOLENBQVA7QUFBdU87QUFBelIsS0FBN0gsRUFBd1o7QUFBQ3pELE1BQUFBLEdBQUcsRUFBQyxxQkFBTDtBQUEyQlUsTUFBQUEsS0FBSyxFQUFDLGVBQVNuQyxDQUFULEVBQVc7QUFBQyxlQUFPMEIsQ0FBQyxDQUFDeUQsd0JBQUYsQ0FBMkJuRixDQUEzQixFQUE4QmtGLE9BQTlCLENBQXNDLE1BQXRDLEVBQTZDLEdBQTdDLEVBQWtEQSxPQUFsRCxDQUEwRCxNQUExRCxFQUFpRSxHQUFqRSxFQUFzRUEsT0FBdEUsQ0FBOEUsTUFBOUUsRUFBcUYsR0FBckYsRUFBMEZBLE9BQTFGLENBQWtHLE1BQWxHLEVBQXlHLEdBQXpHLENBQVA7QUFBcUg7QUFBbEssS0FBeFosRUFBNGpCO0FBQUN6RCxNQUFBQSxHQUFHLEVBQUMsc0JBQUw7QUFBNEJVLE1BQUFBLEtBQUssRUFBQyxlQUFTbkMsQ0FBVCxFQUFXO0FBQUMsZUFBTzBCLENBQUMsQ0FBQ3lELHdCQUFGLENBQTJCbkYsQ0FBM0IsRUFBOEJrRixPQUE5QixDQUFzQyxNQUF0QyxFQUE2QyxHQUE3QyxDQUFQO0FBQXlEO0FBQXZHLEtBQTVqQixDQUEzaUgsQ0FBRCxFQUFtdEl4RCxDQUExdEk7QUFBNHRJLEdBQXAySSxFQUEzbUI7O0FBQWs5SkEsRUFBQUEsQ0FBQyxDQUFDMEQsS0FBRixFQUFRMUQsQ0FBQyxDQUFDMkQsT0FBVjtBQUFrQixNQUFJeEMsQ0FBQyxHQUFDLElBQUluQixDQUFKLEVBQU47QUFBWSxTQUFNO0FBQUNsQixJQUFBQSxNQUFNLEVBQUNrQixDQUFSO0FBQVUvRCxJQUFBQSxPQUFPLEVBQUNrRjtBQUFsQixHQUFOO0FBQTJCLENBQXowSyxDQUFEOzs7Ozs7Ozs7Ozs7QUNBQSIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2Fzc2V0cy9mb2xkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vdmVuZG9yL2ZyaWVuZHNvZnN5bWZvbnkvanNyb3V0aW5nLWJ1bmRsZS9SZXNvdXJjZXMvcHVibGljL2pzL3JvdXRlci5taW4uanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL3N0eWxlcy9mb2xkZXIuc2NzcyJdLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlKCdAZm9ydGF3ZXNvbWUvZm9udGF3ZXNvbWUtZnJlZS9jc3MvYWxsLm1pbi5jc3MnKTtcbnJlcXVpcmUoJ25vdHkvc3JjL25vdHkuc2NzcycpO1xucmVxdWlyZSgnbm90eS9zcmMvdGhlbWVzL3JlbGF4LnNjc3MnKTtcbnJlcXVpcmUoJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1mcmVlL2pzL2FsbC5qcycpO1xuXG5pbXBvcnQgUm91dGluZyBmcm9tICcuLi92ZW5kb3IvZnJpZW5kc29mc3ltZm9ueS9qc3JvdXRpbmctYnVuZGxlL1Jlc291cmNlcy9wdWJsaWMvanMvcm91dGVyLm1pbi5qcyc7XG5pbXBvcnQgTm90eSBmcm9tICdub3R5JztcblxuaW1wb3J0ICcuL3N0eWxlcy9mb2xkZXIuc2Nzcyc7XG5cbiQoXCIucGhhc2VTZWxlY3RcIikub24oJ2NoYW5nZScsIGZ1bmN0aW9uICgpIHtcbiAgICAkLmFqYXgoe1xuICAgICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICAgIHVybDogUm91dGluZy5nZW5lcmF0ZSgndXBkYXRlX3BoYXNlX2ZvbGRlcicsIHtpZDogJCh0aGlzKS5kYXRhKCdmb2xkZXInKX0pLFxuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICBwaGFzZTogJCh0aGlzKS52YWwoKVxuICAgICAgICB9LFxuICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgY29uc3QgdHlwZSA9IChkYXRhKSA/IFwic3VjY2Vzc1wiIDogXCJlcnJvclwiO1xuICAgICAgICAgICAgY29uc3QgdGV4dCA9IChkYXRhKSA/IFwiUGhhc2UgbWlzZSDDoCBqb3VyXCIgOiBcIlVuZSBlcnJldXIgZXN0IHN1cnZlbnVlXCI7XG5cbiAgICAgICAgICAgIG5ldyBOb3R5KHtcbiAgICAgICAgICAgICAgICB0aGVtZTogJ3JlbGF4JyxcbiAgICAgICAgICAgICAgICBsYXlvdXQ6ICd0b3BSaWdodCcsXG4gICAgICAgICAgICAgICAgdGV4dDogdGV4dCxcbiAgICAgICAgICAgICAgICB0eXBlOiB0eXBlLFxuICAgICAgICAgICAgICAgIHByb2dyZXNzQmFyOiB0cnVlLFxuICAgICAgICAgICAgICAgIHRpbWVvdXQ6IDMwMDBcbiAgICAgICAgICAgIH0pLnNob3coKTtcbiAgICAgICAgfVxuICAgIH0pXG59KTtcblxuJChcIiNhZGRfY29tbWVudF9jdGFcIikub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICQoXCIjYWRkX2NvbW1lbnRcIikudG9nZ2xlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICQoXCJodG1sLCBib2R5XCIpLmFuaW1hdGUoe3Njcm9sbFRvcDogJChkb2N1bWVudCkuaGVpZ2h0KCl9LCAxMDAwKTtcbn0pO1xuXG4kKFwiLmRlbGV0ZV9jb21tZW50X2N0YVwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKGNvbmZpcm0oJ8OKdGVzIHZvdXMgc3VyIGRlIHZvdWxvaXIgc3VwcHJpbWVyIGNlIGNvbW1lbnRhaXJlID8nKSkge1xuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogXCJQT1NUXCIsXG4gICAgICAgICAgICB1cmw6IFJvdXRpbmcuZ2VuZXJhdGUoJ2ZvbGRlcl9kZWxldGVfY29tbWVudCcsIHtpZDogJCh0aGlzKS5kYXRhKCdjb21tZW50Jyl9KSxcbiAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufSk7XG5cbiQoXCIuZWRpdF9jb21tZW50X2N0YVwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgY29uc3QgaWRfY29tbWVudCA9ICQodGhpcykuZGF0YSgnY29tbWVudCcpO1xuICAgIGNvbnN0IHBhcmVudERpdiA9ICQoJ2RpdltkYXRhLWlkPVwiJyArIGlkX2NvbW1lbnQgKyAnXCJdJyk7XG4gICAgcGFyZW50RGl2LnRvZ2dsZUNsYXNzKCdkLW5vbmUnKTtcbiAgICAkKFwiI3Bvc3RfY29tbWVudF9waWxvdF9zY29wZVwiKS5hdHRyKCdkaXNhYmxlZCcsIGZ1bmN0aW9uIChfLCBhdHRyKSB7XG4gICAgICAgIHJldHVybiAhYXR0cjtcbiAgICB9KTtcbiAgICAkLmFqYXgoe1xuICAgICAgICB0eXBlOiBcIkdFVFwiLFxuICAgICAgICB1cmw6IFJvdXRpbmcuZ2VuZXJhdGUoJ2dldF9pZHNfc2NvcGVfYnlfY29tbWVudCcsIHtpZDogaWRfY29tbWVudH0pLFxuICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoaWRzKSB7XG4gICAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgICAgIHR5cGU6IFwiR0VUXCIsXG4gICAgICAgICAgICAgICAgdXJsOiBSb3V0aW5nLmdlbmVyYXRlKCdmb2xkZXJfZ2V0X2NvbW1lbnQnLCB7aWQ6IGlkX2NvbW1lbnR9KSxcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAkKFwiI3Bvc3RfY29tbWVudF9waWxvdF9jb21tZW50XCIpLnZhbChkYXRhLmNvbW1lbnQpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zY29wZS5zdGFydHNXaXRoKCdGSScpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI3Bvc3RfY29tbWVudF9waWxvdF9zY29wZVwiKS52YWwoJ0ZJJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuc2NvcGUuc3RhcnRzV2l0aCgnRklTJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjcG9zdF9jb21tZW50X3BpbG90X3Njb3BlXCIpLnZhbCgnRklTJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuc2NvcGUuc3RhcnRzV2l0aCgnSU1CJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjcG9zdF9jb21tZW50X3BpbG90X3Njb3BlXCIpLnZhbCgnaW1iJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAkKFwiI2FkZF9jb21tZW50XCIpLnRvZ2dsZUNsYXNzKCdkLW5vbmUnKTtcbiAgICAgICAgICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7c2Nyb2xsVG9wOiAkKGRvY3VtZW50KS5oZWlnaHQoKX0sIDEwMDApO1xuICAgICAgICAgICAgICAgICAgICAkKFwiI3Bvc3RfY29tbWVudF9waWxvdF9zdWJtaXRcIikudGV4dCgnTW9kaWZpZXInKTtcbiAgICAgICAgICAgICAgICAgICAgJChcIiNwb3N0X2NvbW1lbnRfcGlsb3Rfc3VibWl0XCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiBSb3V0aW5nLmdlbmVyYXRlKCdlZGl0X2ZvbGRlcl9jb21tZW50Jywge2lkOiBpZF9jb21tZW50fSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21tZW50OiAkKFwiI3Bvc3RfY29tbWVudF9waWxvdF9jb21tZW50XCIpLnZhbCgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZHM6IGlkc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfSlcbn0pIiwiIWZ1bmN0aW9uKGUsdCl7dmFyIG49dCgpO1wiZnVuY3Rpb25cIj09dHlwZW9mIGRlZmluZSYmZGVmaW5lLmFtZD9kZWZpbmUoW10sbi5Sb3V0aW5nKTpcIm9iamVjdFwiPT10eXBlb2YgbW9kdWxlJiZtb2R1bGUuZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1uLlJvdXRpbmc6KGUuUm91dGluZz1uLlJvdXRpbmcsZS5mb3M9e1JvdXRlcjpuLlJvdXRlcn0pfSh0aGlzLGZ1bmN0aW9uKCl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gZShlLHQpe2lmKCEoZSBpbnN0YW5jZW9mIHQpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9dmFyIHQ9T2JqZWN0LmFzc2lnbnx8ZnVuY3Rpb24oZSl7Zm9yKHZhciB0PTE7dDxhcmd1bWVudHMubGVuZ3RoO3QrKyl7dmFyIG49YXJndW1lbnRzW3RdO2Zvcih2YXIgbyBpbiBuKU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChuLG8pJiYoZVtvXT1uW29dKX1yZXR1cm4gZX0sbj1cImZ1bmN0aW9uXCI9PXR5cGVvZiBTeW1ib2wmJlwic3ltYm9sXCI9PXR5cGVvZiBTeW1ib2wuaXRlcmF0b3I/ZnVuY3Rpb24oZSl7cmV0dXJuIHR5cGVvZiBlfTpmdW5jdGlvbihlKXtyZXR1cm4gZSYmXCJmdW5jdGlvblwiPT10eXBlb2YgU3ltYm9sJiZlLmNvbnN0cnVjdG9yPT09U3ltYm9sJiZlIT09U3ltYm9sLnByb3RvdHlwZT9cInN5bWJvbFwiOnR5cGVvZiBlfSxvPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gZShlLHQpe2Zvcih2YXIgbj0wO248dC5sZW5ndGg7bisrKXt2YXIgbz10W25dO28uZW51bWVyYWJsZT1vLmVudW1lcmFibGV8fCExLG8uY29uZmlndXJhYmxlPSEwLFwidmFsdWVcImluIG8mJihvLndyaXRhYmxlPSEwKSxPYmplY3QuZGVmaW5lUHJvcGVydHkoZSxvLmtleSxvKX19cmV0dXJuIGZ1bmN0aW9uKHQsbixvKXtyZXR1cm4gbiYmZSh0LnByb3RvdHlwZSxuKSxvJiZlKHQsbyksdH19KCkscj1mdW5jdGlvbigpe2Z1bmN0aW9uIHIodCxuKXtlKHRoaXMsciksdGhpcy5jb250ZXh0Xz10fHx7YmFzZV91cmw6XCJcIixwcmVmaXg6XCJcIixob3N0OlwiXCIscG9ydDpcIlwiLHNjaGVtZTpcIlwiLGxvY2FsZTpcIlwifSx0aGlzLnNldFJvdXRlcyhufHx7fSl9cmV0dXJuIG8ocixbe2tleTpcInNldFJvdXRpbmdEYXRhXCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5zZXRCYXNlVXJsKGUuYmFzZV91cmwpLHRoaXMuc2V0Um91dGVzKGUucm91dGVzKSxcInByZWZpeFwiaW4gZSYmdGhpcy5zZXRQcmVmaXgoZS5wcmVmaXgpLFwicG9ydFwiaW4gZSYmdGhpcy5zZXRQb3J0KGUucG9ydCksXCJsb2NhbGVcImluIGUmJnRoaXMuc2V0TG9jYWxlKGUubG9jYWxlKSx0aGlzLnNldEhvc3QoZS5ob3N0KSx0aGlzLnNldFNjaGVtZShlLnNjaGVtZSl9fSx7a2V5Olwic2V0Um91dGVzXCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5yb3V0ZXNfPU9iamVjdC5mcmVlemUoZSl9fSx7a2V5OlwiZ2V0Um91dGVzXCIsdmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5yb3V0ZXNffX0se2tleTpcInNldEJhc2VVcmxcIix2YWx1ZTpmdW5jdGlvbihlKXt0aGlzLmNvbnRleHRfLmJhc2VfdXJsPWV9fSx7a2V5OlwiZ2V0QmFzZVVybFwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29udGV4dF8uYmFzZV91cmx9fSx7a2V5Olwic2V0UHJlZml4XCIsdmFsdWU6ZnVuY3Rpb24oZSl7dGhpcy5jb250ZXh0Xy5wcmVmaXg9ZX19LHtrZXk6XCJzZXRTY2hlbWVcIix2YWx1ZTpmdW5jdGlvbihlKXt0aGlzLmNvbnRleHRfLnNjaGVtZT1lfX0se2tleTpcImdldFNjaGVtZVwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIHRoaXMuY29udGV4dF8uc2NoZW1lfX0se2tleTpcInNldEhvc3RcIix2YWx1ZTpmdW5jdGlvbihlKXt0aGlzLmNvbnRleHRfLmhvc3Q9ZX19LHtrZXk6XCJnZXRIb3N0XCIsdmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5jb250ZXh0Xy5ob3N0fX0se2tleTpcInNldFBvcnRcIix2YWx1ZTpmdW5jdGlvbihlKXt0aGlzLmNvbnRleHRfLnBvcnQ9ZX19LHtrZXk6XCJnZXRQb3J0XCIsdmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5jb250ZXh0Xy5wb3J0fX0se2tleTpcInNldExvY2FsZVwiLHZhbHVlOmZ1bmN0aW9uKGUpe3RoaXMuY29udGV4dF8ubG9jYWxlPWV9fSx7a2V5OlwiZ2V0TG9jYWxlXCIsdmFsdWU6ZnVuY3Rpb24oKXtyZXR1cm4gdGhpcy5jb250ZXh0Xy5sb2NhbGV9fSx7a2V5OlwiYnVpbGRRdWVyeVBhcmFtc1wiLHZhbHVlOmZ1bmN0aW9uKGUsdCxvKXt2YXIgcj10aGlzLGk9dm9pZCAwLHU9bmV3IFJlZ0V4cCgvXFxbXFxdJC8pO2lmKHQgaW5zdGFuY2VvZiBBcnJheSl0LmZvckVhY2goZnVuY3Rpb24odCxpKXt1LnRlc3QoZSk/byhlLHQpOnIuYnVpbGRRdWVyeVBhcmFtcyhlK1wiW1wiKyhcIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0P1widW5kZWZpbmVkXCI6bih0KSk/aTpcIlwiKStcIl1cIix0LG8pfSk7ZWxzZSBpZihcIm9iamVjdFwiPT09KFwidW5kZWZpbmVkXCI9PXR5cGVvZiB0P1widW5kZWZpbmVkXCI6bih0KSkpZm9yKGkgaW4gdCl0aGlzLmJ1aWxkUXVlcnlQYXJhbXMoZStcIltcIitpK1wiXVwiLHRbaV0sbyk7ZWxzZSBvKGUsdCl9fSx7a2V5OlwiZ2V0Um91dGVcIix2YWx1ZTpmdW5jdGlvbihlKXt2YXIgdD10aGlzLmNvbnRleHRfLnByZWZpeCtlLG49ZStcIi5cIit0aGlzLmNvbnRleHRfLmxvY2FsZSxvPXRoaXMuY29udGV4dF8ucHJlZml4K2UrXCIuXCIrdGhpcy5jb250ZXh0Xy5sb2NhbGUscj1bdCxuLG8sZV07Zm9yKHZhciBpIGluIHIpaWYocltpXWluIHRoaXMucm91dGVzXylyZXR1cm4gdGhpcy5yb3V0ZXNfW3JbaV1dO3Rocm93IG5ldyBFcnJvcignVGhlIHJvdXRlIFwiJytlKydcIiBkb2VzIG5vdCBleGlzdC4nKX19LHtrZXk6XCJnZW5lcmF0ZVwiLHZhbHVlOmZ1bmN0aW9uKGUsbil7dmFyIG89YXJndW1lbnRzLmxlbmd0aD4yJiZ2b2lkIDAhPT1hcmd1bWVudHNbMl0mJmFyZ3VtZW50c1syXSxpPXRoaXMuZ2V0Um91dGUoZSksdT1ufHx7fSxzPXQoe30sdSksYz1cIlwiLGE9ITAsbD1cIlwiLGY9XCJ1bmRlZmluZWRcIj09dHlwZW9mIHRoaXMuZ2V0UG9ydCgpfHxudWxsPT09dGhpcy5nZXRQb3J0KCk/XCJcIjp0aGlzLmdldFBvcnQoKTtpZihpLnRva2Vucy5mb3JFYWNoKGZ1bmN0aW9uKHQpe2lmKFwidGV4dFwiPT09dFswXSlyZXR1cm4gYz1yLmVuY29kZVBhdGhDb21wb25lbnQodFsxXSkrYyx2b2lkKGE9ITEpO3tpZihcInZhcmlhYmxlXCIhPT10WzBdKXRocm93IG5ldyBFcnJvcignVGhlIHRva2VuIHR5cGUgXCInK3RbMF0rJ1wiIGlzIG5vdCBzdXBwb3J0ZWQuJyk7dmFyIG49aS5kZWZhdWx0cyYmdFszXWluIGkuZGVmYXVsdHM7aWYoITE9PT1hfHwhbnx8dFszXWluIHUmJnVbdFszXV0hPWkuZGVmYXVsdHNbdFszXV0pe3ZhciBvPXZvaWQgMDtpZih0WzNdaW4gdSlvPXVbdFszXV0sZGVsZXRlIHNbdFszXV07ZWxzZXtpZighbil7aWYoYSlyZXR1cm47dGhyb3cgbmV3IEVycm9yKCdUaGUgcm91dGUgXCInK2UrJ1wiIHJlcXVpcmVzIHRoZSBwYXJhbWV0ZXIgXCInK3RbM10rJ1wiLicpfW89aS5kZWZhdWx0c1t0WzNdXX12YXIgbD0hMD09PW98fCExPT09b3x8XCJcIj09PW87aWYoIWx8fCFhKXt2YXIgZj1yLmVuY29kZVBhdGhDb21wb25lbnQobyk7XCJudWxsXCI9PT1mJiZudWxsPT09byYmKGY9XCJcIiksYz10WzFdK2YrY31hPSExfWVsc2UgbiYmdFszXWluIHMmJmRlbGV0ZSBzW3RbM11dfX0pLFwiXCI9PT1jJiYoYz1cIi9cIiksaS5ob3N0dG9rZW5zLmZvckVhY2goZnVuY3Rpb24oZSl7dmFyIHQ9dm9pZCAwO3JldHVyblwidGV4dFwiPT09ZVswXT92b2lkKGw9ZVsxXStsKTp2b2lkKFwidmFyaWFibGVcIj09PWVbMF0mJihlWzNdaW4gdT8odD11W2VbM11dLGRlbGV0ZSBzW2VbM11dKTppLmRlZmF1bHRzJiZlWzNdaW4gaS5kZWZhdWx0cyYmKHQ9aS5kZWZhdWx0c1tlWzNdXSksbD1lWzFdK3QrbCkpfSksYz10aGlzLmNvbnRleHRfLmJhc2VfdXJsK2MsaS5yZXF1aXJlbWVudHMmJlwiX3NjaGVtZVwiaW4gaS5yZXF1aXJlbWVudHMmJnRoaXMuZ2V0U2NoZW1lKCkhPWkucmVxdWlyZW1lbnRzLl9zY2hlbWUpe3ZhciBoPWx8fHRoaXMuZ2V0SG9zdCgpO2M9aS5yZXF1aXJlbWVudHMuX3NjaGVtZStcIjovL1wiK2grKGguaW5kZXhPZihcIjpcIitmKT4tMXx8XCJcIj09PWY/XCJcIjpcIjpcIitmKStjfWVsc2UgaWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGkuc2NoZW1lcyYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGkuc2NoZW1lc1swXSYmdGhpcy5nZXRTY2hlbWUoKSE9PWkuc2NoZW1lc1swXSl7dmFyIHA9bHx8dGhpcy5nZXRIb3N0KCk7Yz1pLnNjaGVtZXNbMF0rXCI6Ly9cIitwKyhwLmluZGV4T2YoXCI6XCIrZik+LTF8fFwiXCI9PT1mP1wiXCI6XCI6XCIrZikrY31lbHNlIGwmJnRoaXMuZ2V0SG9zdCgpIT09bCsobC5pbmRleE9mKFwiOlwiK2YpPi0xfHxcIlwiPT09Zj9cIlwiOlwiOlwiK2YpP2M9dGhpcy5nZXRTY2hlbWUoKStcIjovL1wiK2wrKGwuaW5kZXhPZihcIjpcIitmKT4tMXx8XCJcIj09PWY/XCJcIjpcIjpcIitmKStjOm89PT0hMCYmKGM9dGhpcy5nZXRTY2hlbWUoKStcIjovL1wiK3RoaXMuZ2V0SG9zdCgpKyh0aGlzLmdldEhvc3QoKS5pbmRleE9mKFwiOlwiK2YpPi0xfHxcIlwiPT09Zj9cIlwiOlwiOlwiK2YpK2MpO2lmKE9iamVjdC5rZXlzKHMpLmxlbmd0aD4wKXt2YXIgZD12b2lkIDAseT1bXSx2PWZ1bmN0aW9uKGUsdCl7dD1cImZ1bmN0aW9uXCI9PXR5cGVvZiB0P3QoKTp0LHQ9bnVsbD09PXQ/XCJcIjp0LHkucHVzaChyLmVuY29kZVF1ZXJ5Q29tcG9uZW50KGUpK1wiPVwiK3IuZW5jb2RlUXVlcnlDb21wb25lbnQodCkpfTtmb3IoZCBpbiBzKXRoaXMuYnVpbGRRdWVyeVBhcmFtcyhkLHNbZF0sdik7Yz1jK1wiP1wiK3kuam9pbihcIiZcIil9cmV0dXJuIGN9fV0sW3trZXk6XCJnZXRJbnN0YW5jZVwiLHZhbHVlOmZ1bmN0aW9uKCl7cmV0dXJuIGl9fSx7a2V5Olwic2V0RGF0YVwiLHZhbHVlOmZ1bmN0aW9uKGUpe3ZhciB0PXIuZ2V0SW5zdGFuY2UoKTt0LnNldFJvdXRpbmdEYXRhKGUpfX0se2tleTpcImN1c3RvbUVuY29kZVVSSUNvbXBvbmVudFwiLHZhbHVlOmZ1bmN0aW9uKGUpe3JldHVybiBlbmNvZGVVUklDb21wb25lbnQoZSkucmVwbGFjZSgvJTJGL2csXCIvXCIpLnJlcGxhY2UoLyU0MC9nLFwiQFwiKS5yZXBsYWNlKC8lM0EvZyxcIjpcIikucmVwbGFjZSgvJTIxL2csXCIhXCIpLnJlcGxhY2UoLyUzQi9nLFwiO1wiKS5yZXBsYWNlKC8lMkMvZyxcIixcIikucmVwbGFjZSgvJTJBL2csXCIqXCIpLnJlcGxhY2UoL1xcKC9nLFwiJTI4XCIpLnJlcGxhY2UoL1xcKS9nLFwiJTI5XCIpLnJlcGxhY2UoLycvZyxcIiUyN1wiKX19LHtrZXk6XCJlbmNvZGVQYXRoQ29tcG9uZW50XCIsdmFsdWU6ZnVuY3Rpb24oZSl7cmV0dXJuIHIuY3VzdG9tRW5jb2RlVVJJQ29tcG9uZW50KGUpLnJlcGxhY2UoLyUzRC9nLFwiPVwiKS5yZXBsYWNlKC8lMkIvZyxcIitcIikucmVwbGFjZSgvJTIxL2csXCIhXCIpLnJlcGxhY2UoLyU3Qy9nLFwifFwiKX19LHtrZXk6XCJlbmNvZGVRdWVyeUNvbXBvbmVudFwiLHZhbHVlOmZ1bmN0aW9uKGUpe3JldHVybiByLmN1c3RvbUVuY29kZVVSSUNvbXBvbmVudChlKS5yZXBsYWNlKC8lM0YvZyxcIj9cIil9fV0pLHJ9KCk7ci5Sb3V0ZSxyLkNvbnRleHQ7dmFyIGk9bmV3IHI7cmV0dXJue1JvdXRlcjpyLFJvdXRpbmc6aX19KTsiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW5cbmV4cG9ydCB7fTsiXSwibmFtZXMiOlsicmVxdWlyZSIsIlJvdXRpbmciLCJOb3R5IiwiJCIsIm9uIiwiYWpheCIsInR5cGUiLCJ1cmwiLCJnZW5lcmF0ZSIsImlkIiwiZGF0YSIsInBoYXNlIiwidmFsIiwic3VjY2VzcyIsInRleHQiLCJ0aGVtZSIsImxheW91dCIsInByb2dyZXNzQmFyIiwidGltZW91dCIsInNob3ciLCJ0b2dnbGVDbGFzcyIsImFuaW1hdGUiLCJzY3JvbGxUb3AiLCJkb2N1bWVudCIsImhlaWdodCIsImNvbmZpcm0iLCJsb2NhdGlvbiIsInJlbG9hZCIsImlkX2NvbW1lbnQiLCJwYXJlbnREaXYiLCJhdHRyIiwiXyIsImlkcyIsImNvbW1lbnQiLCJzY29wZSIsInN0YXJ0c1dpdGgiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiZSIsInQiLCJuIiwiZGVmaW5lIiwiYW1kIiwibW9kdWxlIiwiZXhwb3J0cyIsImZvcyIsIlJvdXRlciIsIlR5cGVFcnJvciIsIk9iamVjdCIsImFzc2lnbiIsImFyZ3VtZW50cyIsImxlbmd0aCIsIm8iLCJwcm90b3R5cGUiLCJoYXNPd25Qcm9wZXJ0eSIsImNhbGwiLCJTeW1ib2wiLCJpdGVyYXRvciIsImNvbnN0cnVjdG9yIiwiZW51bWVyYWJsZSIsImNvbmZpZ3VyYWJsZSIsIndyaXRhYmxlIiwiZGVmaW5lUHJvcGVydHkiLCJrZXkiLCJyIiwiY29udGV4dF8iLCJiYXNlX3VybCIsInByZWZpeCIsImhvc3QiLCJwb3J0Iiwic2NoZW1lIiwibG9jYWxlIiwic2V0Um91dGVzIiwidmFsdWUiLCJzZXRCYXNlVXJsIiwicm91dGVzIiwic2V0UHJlZml4Iiwic2V0UG9ydCIsInNldExvY2FsZSIsInNldEhvc3QiLCJzZXRTY2hlbWUiLCJyb3V0ZXNfIiwiZnJlZXplIiwiaSIsInUiLCJSZWdFeHAiLCJBcnJheSIsImZvckVhY2giLCJ0ZXN0IiwiYnVpbGRRdWVyeVBhcmFtcyIsIkVycm9yIiwiZ2V0Um91dGUiLCJzIiwiYyIsImEiLCJsIiwiZiIsImdldFBvcnQiLCJ0b2tlbnMiLCJlbmNvZGVQYXRoQ29tcG9uZW50IiwiZGVmYXVsdHMiLCJob3N0dG9rZW5zIiwicmVxdWlyZW1lbnRzIiwiZ2V0U2NoZW1lIiwiX3NjaGVtZSIsImgiLCJnZXRIb3N0IiwiaW5kZXhPZiIsInNjaGVtZXMiLCJwIiwia2V5cyIsImQiLCJ5IiwidiIsInB1c2giLCJlbmNvZGVRdWVyeUNvbXBvbmVudCIsImpvaW4iLCJnZXRJbnN0YW5jZSIsInNldFJvdXRpbmdEYXRhIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwicmVwbGFjZSIsImN1c3RvbUVuY29kZVVSSUNvbXBvbmVudCIsIlJvdXRlIiwiQ29udGV4dCJdLCJzb3VyY2VSb290IjoiIn0=