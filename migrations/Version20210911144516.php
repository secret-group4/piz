<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911144516 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE departments (id INT AUTO_INCREMENT NOT NULL, etr_id INT NOT NULL, department INT NOT NULL, INDEX IDX_16AEB8D4BD9F0D70 (etr_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE departments ADD CONSTRAINT FK_16AEB8D4BD9F0D70 FOREIGN KEY (etr_id) REFERENCES etr (id)');
        $this->addSql('ALTER TABLE zones RENAME INDEX idx_a0ebc007283e3c09 TO IDX_85CAB168283E3C09');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE departments');
        $this->addSql('ALTER TABLE zones RENAME INDEX idx_85cab168283e3c09 TO IDX_A0EBC007283E3C09');
    }
}
