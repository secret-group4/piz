<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911124632 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sous_ju ADD zone_id INT NOT NULL');
        $this->addSql('ALTER TABLE sous_ju ADD CONSTRAINT FK_296BA7999F2C3FAB FOREIGN KEY (zone_id) REFERENCES zones (id)');
        $this->addSql('CREATE INDEX IDX_296BA7999F2C3FAB ON sous_ju (zone_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sous_ju DROP FOREIGN KEY FK_296BA7999F2C3FAB');
        $this->addSql('DROP INDEX IDX_296BA7999F2C3FAB ON sous_ju');
        $this->addSql('ALTER TABLE sous_ju DROP zone_id');
    }
}
