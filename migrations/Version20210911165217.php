<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911165217 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE folders (id INT AUTO_INCREMENT NOT NULL, etr_id INT NOT NULL, zone_id INT NOT NULL, as_number VARCHAR(50) DEFAULT NULL, regroupement VARCHAR(13) DEFAULT NULL, imb VARCHAR(16) NOT NULL, type_zlin VARCHAR(255) NOT NULL, etat_zlin VARCHAR(255) DEFAULT NULL, etat_j3m VARCHAR(255) DEFAULT NULL, etat_reseau VARCHAR(255) NOT NULL, accord_promo VARCHAR(255) NOT NULL, etat_nego VARCHAR(255) NOT NULL, statut_pose_pb VARCHAR(255) NOT NULL, etat_installation VARCHAR(255) NOT NULL, dlpi DATE NOT NULL, prod VARCHAR(255) DEFAULT NULL, bloque VARCHAR(255) DEFAULT NULL, el INT NOT NULL, ville VARCHAR(255) NOT NULL, numero_voie INT DEFAULT NULL, voie VARCHAR(255) NOT NULL, complement_voie VARCHAR(255) DEFAULT NULL, syndic VARCHAR(255) DEFAULT NULL, ingenierie VARCHAR(3) NOT NULL, densite VARCHAR(2) DEFAULT NULL, cle_site VARCHAR(6) NOT NULL, etat_pa VARCHAR(255) DEFAULT NULL, code_pa VARCHAR(255) DEFAULT NULL, etat_pm VARCHAR(255) DEFAULT NULL, fi VARCHAR(13) DEFAULT NULL, arcep_pm DATE DEFAULT NULL, date_pose_pb DATE DEFAULT NULL, date_racco DATE DEFAULT NULL, arcep_site DATE DEFAULT NULL, mesc_ft DATE DEFAULT NULL, j3m DATE DEFAULT NULL, date_accord DATE DEFAULT NULL, dpli_initial DATE NOT NULL, etat_z4 INT DEFAULT NULL, j3m_impact INT NOT NULL, pm_impact INT NOT NULL, site_impact INT NOT NULL, folder_status VARCHAR(255) NOT NULL, close TINYINT(1) NOT NULL, el_fis INT DEFAULT NULL, caff VARCHAR(255) DEFAULT NULL, caff_ref VARCHAR(255) DEFAULT NULL, oeie VARCHAR(255) DEFAULT NULL, acteur VARCHAR(255) DEFAULT NULL, priority TINYINT(1) NOT NULL, flux_frais TINYINT(1) NOT NULL, reacol VARCHAR(255) NOT NULL, forced_etr TINYINT(1) NOT NULL, incoherence_etr TINYINT(1) NOT NULL, present_import TINYINT(1) NOT NULL, phase VARCHAR(255) DEFAULT NULL, etr_optimum VARCHAR(255) DEFAULT NULL, col_immo VARCHAR(255) DEFAULT NULL, forced_column TINYINT(1) NOT NULL, INDEX IDX_FE37D30FBD9F0D70 (etr_id), INDEX IDX_FE37D30F9F2C3FAB (zone_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE folders ADD CONSTRAINT FK_FE37D30FBD9F0D70 FOREIGN KEY (etr_id) REFERENCES etr (id)');
        $this->addSql('ALTER TABLE folders ADD CONSTRAINT FK_FE37D30F9F2C3FAB FOREIGN KEY (zone_id) REFERENCES zones (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE folders');
    }
}
