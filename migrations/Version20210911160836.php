<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911160836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE department_exception (id INT AUTO_INCREMENT NOT NULL, etr_id INT NOT NULL, department_id INT NOT NULL, insee VARCHAR(5) NOT NULL, INDEX IDX_A44BF9A1BD9F0D70 (etr_id), INDEX IDX_A44BF9A1AE80F5DF (department_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE department_exception ADD CONSTRAINT FK_A44BF9A1BD9F0D70 FOREIGN KEY (etr_id) REFERENCES etr (id)');
        $this->addSql('ALTER TABLE department_exception ADD CONSTRAINT FK_A44BF9A1AE80F5DF FOREIGN KEY (department_id) REFERENCES departments (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE department_exception');
    }
}
