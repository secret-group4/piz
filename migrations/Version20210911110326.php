<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911110326 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sous_ju (id INT AUTO_INCREMENT NOT NULL, activity_id INT NOT NULL, INDEX IDX_296BA79981C06096 (activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE zones (id INT AUTO_INCREMENT NOT NULL, ui_id INT NOT NULL, name VARCHAR(255) NOT NULL, departments LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', base_gdp VARCHAR(2) NOT NULL, INDEX IDX_A0EBC007283E3C09 (ui_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sous_ju ADD CONSTRAINT FK_296BA79981C06096 FOREIGN KEY (activity_id) REFERENCES activities (id)');
        $this->addSql('ALTER TABLE zones ADD CONSTRAINT FK_A0EBC007283E3C09 FOREIGN KEY (ui_id) REFERENCES ui (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE sous_ju');
        $this->addSql('DROP TABLE zones');
    }
}
