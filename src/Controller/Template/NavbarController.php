<?php

namespace App\Controller\Template;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NavbarController extends AbstractController
{
    public function nav(): Response
    {
        return $this->render('navbar/index.html.twig');
    }
}
