<?php

namespace App\Form;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'attr' => [
                    'class' => 'form-control mb-3',
                    'placeholder' => 'Prénom',
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ],
                'label' => 'Prénom'
            ])
            ->add('lastname', TextType::class, [
                'attr' => [
                    'class' => 'form-control mb-3',
                    'placeholder' => 'Nom de famille',
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ],
                'label' => 'Nom de famille'
            ])
            ->add('cuid', TextType::class, [
                'attr' => [
                    'class' => 'form-control mb-3',
                    'placeholder' => 'CUID (ABCD1234)',
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'form-control mb-3',
                    'placeholder' => 'prenom.nom@orange.com',
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ]
            ])
            ->add('active', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-select mb-3',
                    'placeholder' => 'prenom.nom@orange.com',
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ],
                'choices' => [
                    'Oui' => 1,
                    'Non' => 0
                ],
                'label' => 'Actif'
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'attr' => [
                        'class' => 'form-control mb-3',
                        'placeholder' => '************'
                    ],
                    'label_attr' => [
                        'class' => 'form_label',
                    ],
                    'label' => 'Mot de passe'
                ],
                'second_options' => [
                    'attr' => [
                        'class' => 'form-control mb-3',
                        'placeholder' => '************'
                    ],
                    'label_attr' => [
                        'class' => 'form_label',
                    ],
                    'label' => 'Mot de passe (Confirmation)'
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'attr' => [
                    'class' => 'form-select',
                    'placeholder' => 'Role'
                ],
                'label_attr' => [
                    'class' => 'form_label'
                ],
                'choices' => [
                    'Administrateur' => 'ROLE_ADMIN',
                    'Manager' => 'ROLE_MANAGER',
                    'Utilisateur' => 'ROLE_USER',
                ],
                'multiple' => true
            ])
            ->add('manager', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $repository) {
                    return $repository->createQueryBuilder('u')
                        ->orderBy('u.roles', 'ASC')
                        ->andWhere('u.roles LIKE :role')
                        ->setParameter('role', '%ROLE_MANAGER%');
                },
                'choice_label' => function(User $user) {
                    return strtoupper($user->getLastname()) . ' '. $user->getFirstname();
                },
                'attr' => [
                    'class' => 'form-select mb-3'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Créer',
                'attr' => [
                    'class' => 'btn btn-primary w-100'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
