<?php

namespace App\Command;

use App\Repository\FolderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CronUpdateElFisCommand extends Command
{
    protected static $defaultName = 'cron:update-el-fis';
    protected static $defaultDescription = 'Add a short description for your command';

    /** @var FolderRepository */
    private FolderRepository $folderRepository;
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    public function __construct(FolderRepository $folderRepository,EntityManagerInterface $em, string $name = null)
    {
        $this->folderRepository = $folderRepository;
        $this->em = $em;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $io = new SymfonyStyle($input, $output);
        $io->info('Début de la mise à jour ETR à ' . date('d/m/Y H:i:s'));
        $data = $this->folderRepository->getFoldersWithRegroupement();
        $size = count($data);
        $batchSize = 20;
        $i = 1;

        $progress = new ProgressBar($output, $size);
        $progress->start();
        foreach ($data as $folder) {
            $folder->setElFis($this->folderRepository->countElFis($folder));
            $this->em->persist($folder);
            if (($i % $batchSize) === 0) {
                $this->em->flush();
                // Detaches all objects from Doctrine for memory save
                $this->em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);
            }
            $i++;
        }
        $this->em->flush();
        $this->em->clear();
        $progress->finish();
        $io->info('FIN de la mise à jour ETR à ' . date('d/m/Y H:i:s'));

        return Command::SUCCESS;
    }
}
