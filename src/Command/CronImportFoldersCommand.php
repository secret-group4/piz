<?php

namespace App\Command;

use App\Entity\Etr;
use App\Entity\Folder;
use App\Entity\Regroupement;
use App\Entity\Signalisation;
use App\Entity\Zone;
use App\Repository\EtrRepository;
use App\Repository\FolderRepository;
use App\Repository\RegroupementRepository;
use App\Repository\SignlisationRepository;
use App\Repository\ZoneRepository;
use App\Service\FileService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CronImportFoldersCommand extends Command
{
    protected static $defaultName = 'cron:import-folders';
    protected static $defaultDescription = 'Add a short description for your command';

    private const DEFAULT_DATE = '1970-01-01';

    private EntityManagerInterface $em;
    private FileService $file;
    private ContainerInterface $container;
    private FolderRepository $folderRepository;
    private ZoneRepository $zoneRepository;
    private EtrRepository $etrRepository;

    public function __construct(EntityManagerInterface $em, FileService $file, ContainerInterface $container, FolderRepository $folderRepository, EtrRepository $etrRepository, ZoneRepository $zoneRepository, string $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->file = $file;
        $this->container = $container;
        $this->folderRepository = $folderRepository;
        $this->etrRepository = $etrRepository;
        $this->zoneRepository = $zoneRepository;
    }

    /**
     * @throws NonUniqueResultException
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $io = new SymfonyStyle($input, $output);

        $io->info('Début de l\'importation du fichier Optimum à ' . date('d/m/Y H:i:s'));

        $spreadSheet = $this->file->readFile($this->container->getParameter('cron_files_directory') . '/' . 'folders.xlsx');
        $data = $this->file->createDataFromSpreadSheet($spreadSheet);

        $size = count($data);
        $batchSize = 20;
        $i = 1;

        $progress = new ProgressBar($output, $size);
        $progress->start();

        foreach ($data as $row) {
            $folder = $this->folderRepository->findOneBy(['imb' => $row[4]]);

            if (is_null($folder)) {
                $newData[] = $row;
                // Not existing in table
                $folder = (new Folder())
                    ->setImb($row[4])
                    ->setDlpiInitial(Date::excelToDateTimeObject($row[14]))
                    ->setEtatZ4(null)
                    ->setClose(false)
                    ->setFluxFrais(true)
                    ->setForcedEtr(false)
                    ->setForcedColumn(false)
                    ->setPriority(0);
            }

            if ($zone = ($this->zoneRepository->getZoneByDepartment(substr($row[4], 4, 5)))) {
                $folder->setZone($zone);
            } else {
                $folder->setZone($this->zoneRepository->getZoneByDepartment(substr($row[4], 4, 2)));
            }
            $folder
                ->setEtr(
                    $this->etrRepository->findOneBy(['name' => '__'])
                )
                ->setAsNumber($row[2])
                ->setRegroupement($row[3])
                ->setTypeZlin($row[5])
                ->setEtatZlin($row[6])
                ->setEtrOptimum($row[7])
                ->setEtatJ3m($row[8])
                ->setEtatReseau($row[9])
                ->setAccordPromo($row[10])
                ->setEtatNego($row[11])
                ->setStatutPosePb($row[12])
                ->setEtatInstallation($row[13])
                ->setDlpi(Date::excelToDateTimeObject($row[14]))
                ->setProd($row[15])
                ->setBloque($row[16])
                ->setEl($row[17])
                ->setVille($row[20])
                ->setNumeroVoie($row[21])
                ->setVoie($row[22] . " " . $row[23])
                ->setComplementVoie($row[24])
                ->setSyndic($row[25])
                ->setIngenierie($row[26])
                ->setDensite($row[27])
                ->setCleSite($row[28])
                ->setEtatPa($row[29])
                ->setCodePa($row[30])
                ->setEtatPm($row[31])
                ->setFi($row[32])
                ->setArcepPm((Date::excelToDateTimeObject($row[33]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[33])))
                ->setDatePosePb(Date::excelToDateTimeObject($row[35]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[35]))
                ->setDateRacco(Date::excelToDateTimeObject($row[36]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[36]))
                ->setArcepSite(Date::excelToDateTimeObject($row[37]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[37]))
                ->setMescFt(Date::excelToDateTimeObject($row[38]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[38]))
                ->setJ3m(Date::excelToDateTimeObject($row[39]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[39]))
                ->setDateAccord(Date::excelToDateTimeObject($row[40]) == self::DEFAULT_DATE ? null : Date::excelToDateTimeObject($row[40]))
                ->setElFis(0) // TODO
                ->setPresentImport(true)
                ->setIncoherenceEtr(false) // TODO
                ->setColImmo('TODO');// TODO

            $this->em->persist($folder);
            if (($i % $batchSize) === 0) {
                $this->em->flush();
                // Detaches all objects from Doctrine for memory save
                $this->em->clear();

                // Advancing for progress display on console
                $progress->advance($batchSize);
            }
            $i++;
        }
        $this->em->flush();
        $this->em->clear();
        $progress->finish();
    }
}
