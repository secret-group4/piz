<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture implements OrderedFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $manager->persist((new User())
            ->setActive(true)
            ->setLastname('Admin')
            ->setFirstname('Admin')
            ->setCUID('ADMI1234')
            ->setEmail('admin.admin@orange.com')
            ->setPassword('ADMI1234') // password = password
            ->setRoles(['ROLE_ADMIN'])
        );

        $manager->persist((new User())
            ->setActive(true)
            ->setLastname('CONFIG')
            ->setFirstname('CONFIG')
            ->setCUID('CONFI1234')
            ->setEmail('config.config@orange.com')
            ->setPassword('CONFI1234') // password = password
            ->setRoles(['ROLE_CONFIGURATION'])
        );

        $manager->persist((new User())
            ->setActive(true)
            ->setLastname('User')
            ->setFirstname('User')
            ->setCUID('USER1234')
            ->setEmail('user.user@orange.com')
            ->setPassword('USER1234') // password = password
            ->setRoles(['ROLE_USER'])
        );

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }

    public static function getGroups(): array
    {
        return ['main', 'security'];
    }
}
